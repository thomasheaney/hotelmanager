﻿using HotelManager.Models;
using HotelManager.Models.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.ViewModels
{
    public class vmAccommodationDetail
    {
        public Accommodation Accommodation { get; set; }
        public SletohHotel SletohHotel { get; set; }
        public ExpediaHotel ExpediaHotel { get; set; }
    }
}