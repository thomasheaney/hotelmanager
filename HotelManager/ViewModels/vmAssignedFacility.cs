﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.ViewModels
{
    public class vmAssignedFacility
    {
        public int FacilityID {get; set;}
        public string FacilityName { get; set; }
        public bool Assigned { get; set; }
    }
}