﻿using HotelManager.Models.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.ViewModels
{
    public class vmImportChanges
    {
        public List<ImportChanges> Changes { get; set; }
        public bool ChangesFound {  get; set;}
    }
}