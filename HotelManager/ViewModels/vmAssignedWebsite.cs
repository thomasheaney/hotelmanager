﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.ViewModels
{
    public class vmAssignedWebsite
    {
        public int WebsiteID {get; set;}
        public string WebsiteName { get; set; }
        public bool Assigned { get; set; }
    }
}