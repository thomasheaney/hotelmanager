﻿using HotelManager.Models.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.ViewModels
{
    public class vmSletohImport
    {
        public List<SletohHotel> SletohHotels { get; set; }
        public List<SletohFile> Fails { get; set; }
        public List<String> ErrorMessages { get; set; }
        public int ProcessedCount { get; set; }
        public int UnchangedCount { get; set; }
    }
}