﻿using HotelManager.Models.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.ViewModels
{
    public class vmExpediaImport
    {
        public List<ExpediaHotel> ExpediaHotels { get; set; }
        public List<ActivePropertyList> Fails { get; set; }
        public List<String> ErrorMessages { get; set; }
        public int ProcessedCount { get; set; }
        public int UnchangedCount { get; set; }
    }
}