﻿using HotelManager.DAL;
using HotelManager.Models;
using HotelManager.Models.Logging;
using HotelManager.Models.Providers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HotelManager.Helpers.Providers
{
    public class ExpediaHelpers
    {
        public static int GetOrphanedHotelsCount()
        {
            HotelManagerContext context = new DAL.HotelManagerContext();
            var OrphanedHotels = context.ExpediaHotels.Where(h => h.InMasterList == false).Count();

            return OrphanedHotels;
        }

        public static bool AddHotelToMasterList(int accommodationID, ExpediaHotel hotel)
        {
            HotelManagerContext context = new HotelManagerContext();

            Accommodation accommodation = context.Accommodations.Find(accommodationID);
            ExpediaHotel matchedExpediaHotel = context.ExpediaHotels.Find(hotel.ID);

            if (accommodation != null && matchedExpediaHotel != null)
            {
                matchedExpediaHotel.InMasterList = true;
                matchedExpediaHotel.FlaggedForReview = false;
                context.Entry(matchedExpediaHotel).State = EntityState.Modified;

                // Update the master list expedia hotel refernce field
                accommodation.ExpediaHotel = matchedExpediaHotel.ProviderHotelID;
                accommodation.Latitude = matchedExpediaHotel.Latitude;
                accommodation.Longitude = matchedExpediaHotel.Longitude;
                accommodation.NearestAirportCode = matchedExpediaHotel.NearestAirportCode;
                accommodation.HighRate = matchedExpediaHotel.HighRate;
                accommodation.LowRate = matchedExpediaHotel.LowRate;
                accommodation.CheckInTime = matchedExpediaHotel.CheckInTime;
                accommodation.CheckOutTime = matchedExpediaHotel.CheckOutTime;

                context.Entry(accommodation).State = EntityState.Modified;

                // Log changes
                ImportChanges changeLog = new ImportChanges
                {
                    Provider = "2",
                    item_ID = accommodationID,
                    FieldName = "ExpediaHotel",
                    OriginalValue = "Manually assigned Expedia Hotel to master list",
                    NewValue = matchedExpediaHotel.ProviderHotelID.ToString()

                };
                context.ImportChanges.Add(changeLog);
                context.SaveChanges();
                return true;

            }
            else
            {
                if (accommodation != null)
                {
                    // Log changes
                    ImportChanges changeLog = new ImportChanges
                    {
                        Provider = "",
                        item_ID = accommodationID,
                        FieldName = "ID",
                        OriginalValue = "Manually assigned Expedia Hotel to master list",
                        NewValue = "Master hotel not found."

                    };
                    context.ImportChanges.Add(changeLog);
                }

                if (matchedExpediaHotel != null)
                {
                    // Log changes
                    ImportChanges changeLog = new ImportChanges
                    {
                        Provider = "2",
                        item_ID = matchedExpediaHotel.ID,
                        FieldName = "ID",
                        OriginalValue = "Manually assigned Expedia Hotel to master list",
                        NewValue = "Expedia hotel not found."

                    };
                    context.ImportChanges.Add(changeLog);
                }
                context.SaveChanges();
            }

            return false;
        }

        public static bool AddNewHotelToMasterList(ExpediaHotel hotel)
        {
            HotelManagerContext context = new HotelManagerContext();

            ExpediaHotel matchedExpediaHotel = context.ExpediaHotels.Find(hotel.ID);

            if ( matchedExpediaHotel != null)
            {

                // Check for existing page slug, if exists then add postcode to slug
                var slug = presentationHelper.Slugify(matchedExpediaHotel.HotelName + "-" + matchedExpediaHotel.TownName);
                var slugExists = context.Accommodations.Where(a => a.PageSlug == slug).Count() > 0 ? true : false;
                if (slugExists) { slug = slug + "-" + presentationHelper.Slugify(matchedExpediaHotel.PostalCode); }

                // Try and find the town, if it doesnt exist use "Unknown"
                var town = ImportHelpers.FindTownFromString(hotel.TownName);
                if (town == null) { town = ImportHelpers.FindTownFromString("Unknown"); }


                var accommodation = new Accommodation()
                {
                    AccommodationName = matchedExpediaHotel.HotelName,
                    AddressLine1 = matchedExpediaHotel.AddressLine1,
                    AddressLine2 = matchedExpediaHotel.AddressLine2,
                    PostalCode = matchedExpediaHotel.PostalCode,
                    TownID = town.ID,
                    ExpediaHotel = matchedExpediaHotel.ProviderHotelID,
                    PageSlug = slug,
                    StarRating = matchedExpediaHotel.StarRating,

                    Latitude = matchedExpediaHotel.Latitude,
                    Longitude = matchedExpediaHotel.Longitude,

                    NearestAirportCode = matchedExpediaHotel.NearestAirportCode,
                    HighRate = matchedExpediaHotel.HighRate,
                    LowRate = matchedExpediaHotel.LowRate ,
                    CheckInTime = matchedExpediaHotel.CheckInTime,
                    CheckOutTime = matchedExpediaHotel.CheckOutTime
                    
                };


                matchedExpediaHotel.InMasterList = true;
                matchedExpediaHotel.FlaggedForReview = false;
                context.Entry(matchedExpediaHotel).State = EntityState.Modified;

                // Log changes
                ImportChanges changeLog = new ImportChanges
                {
                    Provider = "2",
                    item_ID = 0,
                    FieldName = "ExpediaHotel",
                    OriginalValue = "Manually added Expedia Hotel to master list",
                    NewValue = matchedExpediaHotel.ProviderHotelID.ToString()

                };


                context.Accommodations.Add(accommodation);
                context.ImportChanges.Add(changeLog);

                context.SaveChanges();
                return true;
            }
            else
            {
                if (matchedExpediaHotel != null)
                {
                    // Log changes
                    ImportChanges changeLog = new ImportChanges
                    {
                        Provider = "2",
                        item_ID = matchedExpediaHotel.ID,
                        FieldName = "ID",
                        OriginalValue = "Manually assigned Expedia Hotel to master list",
                        NewValue = "Expedia hotel not found."

                    };
                    context.ImportChanges.Add(changeLog);
                }
                context.SaveChanges();
                return false;
            }
        }
    }
}