﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace HotelManager.Helpers
{
    public class presentationHelper
    {
        public static string getPostalArea(string postCode)
        {
            string[] segments = postCode.Split(' ');
            return Regex.Replace(segments[0], @"[\d-]", string.Empty);
        }

        public static string Slugify(string title)
        {
            string str = title.ToLower();

            // Special case, we want this to appear in the url
            str = str.Replace("b&amp;b", "b-and-b");

            // remove HTML character entities
            str = HttpUtility.HtmlDecode(str);

            // get rid of hyphens so the route doesn’t break
            str = str.Replace("-", " ");
            // invalid chars          
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space  
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim
            //str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        public static string toCSSClassName(string target)
        {

            StringBuilder sb = new StringBuilder(target.Length);
            // Lower the first char.
            sb.Append(char.ToLower(target[0]));
            for (int i = 1; i < target.Length; i++)
            {
                // Get the current char.
                char c = target[i];
                c = char.ToLower(c);

                sb.Append(c);
            }

            sb.Replace("&", "and");
            sb.Replace(".", "");
            sb.Replace(",", "");
            sb.Replace("(", "");
            sb.Replace(")", "");
            sb.Replace(" ", "-");

            return sb.ToString();
        }

        public static string toCSSClassName(string target, string filter)
        {
            string output = presentationHelper.toCSSClassName(target);
            return output.Replace(filter, "");
        }

        public static string stripHTML(string html, bool allowHarmlessTags)
        {
            if (html == null || html == string.Empty)
                return string.Empty;
            if (allowHarmlessTags)
                return System.Text.RegularExpressions.Regex.Replace(html, "", string.Empty);

            return System.Text.RegularExpressions.Regex.Replace(html, "<[^>]*>", string.Empty);
        }

        public static string buildStatisticsRow(string value, string cssClass, string title, string suffix = "", bool hideEmpty = true, bool isDate = false)
        {

            string[] conditions = new string[] { "noshow", "0" };
            bool hasValue = (value != null && value != "" && Array.IndexOf(conditions, value.ToLower()) == -1) ? true : false;

            if (hasValue || !hideEmpty)
            {
                if (isDate)
                {
                    try { value = DateTime.Parse(value).ToString("yyyy"); }
                    catch { value = "--"; }
                }
                return "<li class=\"" + cssClass + "\"><span class=\"title\">"
                    + title + "</span>"
                    + (hasValue ? value : "--") + (suffix != "" && hasValue ? suffix : "") + "</li>";
            }
            else
            {
                return "";
            }

        }

        public static string addSuperscript(string target)
        {

            StringBuilder sb = new StringBuilder(target.Length);
            // Lower the first char.
            for (int i = 0; i < target.Length; i++)
            {
                // Get the current char.
                char c = target[i];

                if (c == 174)
                {
                    sb.Append("<sup>" + c + "</sup>");
                }
                else
                { sb.Append(c); }
            }

            return sb.ToString();
        }


        public static string GetVimeoPreviewImage(string vimeoID)
        {
            try
            {
                //string vimeoUrl = System.Web.HttpContext.Current.Server.HtmlEncode(vimeoURL);
                //int pos = vimeoUrl.LastIndexOf(".com");
                //string videoID = vimeoUrl.Substring(pos + 5, 8);

                XmlDocument doc = new XmlDocument();
                doc.Load("http://vimeo.com/api/v2/video/" + vimeoID + ".xml");
                XmlElement root = doc.DocumentElement;
                string vimeoThumb = root.FirstChild.SelectSingleNode("thumbnail_large").ChildNodes[0].Value;
                string imageURL = vimeoThumb;
                return imageURL;
            }
            catch
            {
                /*string vimeoUrl = System.Web.HttpContext.Current.Server.HtmlEncode(vimeoURL);
                int pos = vimeoUrl.LastIndexOf(".com");
                string videoID = vimeoUrl.Substring(pos + 5, 8);
                return "http://vimeo.com/api/v2/video/" + videoID + ".xml";
                 * */
                return null;
            }
        }

        public static string Truncate(string source, int length, bool useEllipsis = false)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
                if (useEllipsis)
                {
                    source = source + "&hellip;";
                }
            }


            return source;
        }
    }

}
    
    
