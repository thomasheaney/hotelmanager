﻿using HotelManager.DAL;
using HotelManager.Models;
using HotelManager.Models.Logging;
using HotelManager.Models.Providers;
using HotelManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace HotelManager.Helpers
{
    public class ImportHelpers
    {

        public static vmImportChanges compareItems(ProviderHotel existingItem, ProviderHotel newItem)
        {
            bool differencesFound = false;
            string providerID = existingItem.ProviderID.ToString();
            List<ImportChanges> chchchchanges = new List<ImportChanges>();

            if (existingItem == null || newItem == null)
            {
                throw new System.InvalidOperationException("Cannot compare a null object");
            }
            Type firstType = existingItem.GetType();
            if (newItem.GetType() != firstType)
            {
                throw new System.InvalidOperationException("Cannot compare different object types");
            }

            foreach (PropertyInfo propertyInfo in firstType.GetProperties())
            {
                if (propertyInfo.CanRead && propertyInfo.Name !="ID")
                {
                    object firstValue = propertyInfo.GetValue(existingItem, null);
                    object secondValue = propertyInfo.GetValue(newItem, null);
                    if (!object.Equals(firstValue, secondValue))
                    {
                        differencesFound = true;
                        ImportChanges change = new ImportChanges
                        {
                            Provider = providerID,
                            item_ID = existingItem.ProviderHotelID,
                            FieldName = propertyInfo.Name,
                            OriginalValue = firstValue.ToString(),
                            NewValue = secondValue.ToString()

                        };

                        chchchchanges.Add(change);
                    }
                }
            }

            var viewModel = new vmImportChanges
            {
                Changes = chchchchanges,
                ChangesFound = differencesFound

            };

            return viewModel;

        }

        public static Town FindTownFromString(string townString)
        {
            HotelManagerContext db = new HotelManagerContext();
            Town town = db.Towns.SingleOrDefault(p => p.TownName.ToLower().Equals(townString.ToLower()));

            if(town != null)
            { return town;}
            else
            { return null; }
        }

        public static int StringDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

    }
}