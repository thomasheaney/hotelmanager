﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HotelManager
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "Regions",
               url: "Regions/{slug}",
               defaults: new { controller = "Regions", action = "Details" }
           );

            routes.MapRoute(
             name: "Towns",
             url: "Towns/{slug}",
             defaults: new { controller = "Towns", action = "Details" }
         );

            routes.MapRoute(
             name: "Accommodations",
             url: "Accommodations/{slug}",
             defaults: new { controller = "Accommodations", action = "Details" }
         );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
