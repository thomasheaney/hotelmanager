namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAccommodationFacilitiesRelationship : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccommodationFacility",
                c => new
                    {
                        AccommodationID = c.Int(nullable: false),
                        FacilityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccommodationID, t.FacilityID })
                .ForeignKey("dbo.Accommodation", t => t.AccommodationID, cascadeDelete: true)
                .ForeignKey("dbo.Facility", t => t.FacilityID, cascadeDelete: true)
                .Index(t => t.AccommodationID)
                .Index(t => t.FacilityID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccommodationFacility", "FacilityID", "dbo.Facility");
            DropForeignKey("dbo.AccommodationFacility", "AccommodationID", "dbo.Accommodation");
            DropIndex("dbo.AccommodationFacility", new[] { "FacilityID" });
            DropIndex("dbo.AccommodationFacility", new[] { "AccommodationID" });
            DropTable("dbo.AccommodationFacility");
        }
    }
}
