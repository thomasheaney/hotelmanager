namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accommodation",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Slug = c.String(),
                        Name = c.String(),
                        AddressStreet = c.String(),
                        AddressTown = c.String(),
                        AddressPostcode = c.String(),
                        TownID = c.Int(nullable: false),
                        TownSlug = c.String(),
                        TownName = c.String(),
                        Latitude = c.Single(),
                        Longitude = c.Single(),
                        StarRating = c.Single(),
                        Summary = c.String(),
                        Desctiption = c.String(),
                        mainThumbnail = c.String(),
                        mainImage = c.String(),
                        sletohLink = c.String(),
                        show = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Town", t => t.TownID, cascadeDelete: true)
                .Index(t => t.TownID);
            
            CreateTable(
                "dbo.Town",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Slug = c.String(),
                        RegionID = c.Int(nullable: false),
                        RegionSlug = c.String(),
                        RegionName = c.String(),
                        TownName = c.String(),
                        TownSummary = c.String(),
                        TownDescription = c.String(),
                        SletohID = c.Int(nullable: false),
                        Show = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Region", t => t.RegionID, cascadeDelete: true)
                .Index(t => t.RegionID);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Slug = c.String(),
                        RegionName = c.String(),
                        RegionSummary = c.String(),
                        RegionDescription = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Town", "RegionID", "dbo.Region");
            DropForeignKey("dbo.Accommodation", "TownID", "dbo.Town");
            DropIndex("dbo.Town", new[] { "RegionID" });
            DropIndex("dbo.Accommodation", new[] { "TownID" });
            DropTable("dbo.Region");
            DropTable("dbo.Town");
            DropTable("dbo.Accommodation");
        }
    }
}
