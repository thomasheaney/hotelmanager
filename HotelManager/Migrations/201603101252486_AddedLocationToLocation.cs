namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLocationToLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Region", "LocationID", c => c.Int(nullable: false));
            CreateIndex("dbo.Region", "LocationID");
            AddForeignKey("dbo.Region", "LocationID", "dbo.Location", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Region", "LocationID", "dbo.Location");
            DropIndex("dbo.Region", new[] { "LocationID" });
            DropColumn("dbo.Region", "LocationID");
        }
    }
}
