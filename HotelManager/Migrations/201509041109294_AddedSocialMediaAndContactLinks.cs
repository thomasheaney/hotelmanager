namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSocialMediaAndContactLinks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accommodation", "AccommodationWebsite", c => c.String());
            AddColumn("dbo.Accommodation", "AccommodationFacebookLink", c => c.String());
            AddColumn("dbo.Accommodation", "AccommodationTwitterLink", c => c.String());
            AddColumn("dbo.Accommodation", "AccommodationContactEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accommodation", "AccommodationContactEmail");
            DropColumn("dbo.Accommodation", "AccommodationTwitterLink");
            DropColumn("dbo.Accommodation", "AccommodationFacebookLink");
            DropColumn("dbo.Accommodation", "AccommodationWebsite");
        }
    }
}
