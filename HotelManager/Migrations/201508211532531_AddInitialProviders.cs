namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInitialProviders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExpediaHotel",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        hotel_id = c.Int(),
                        sequence_number = c.Int(),
                        name = c.String(),
                        address_1 = c.String(),
                        address_2 = c.String(),
                        city = c.String(),
                        state_province = c.String(),
                        postal_code = c.String(),
                        country = c.String(),
                        latitude = c.Single(),
                        longitude = c.Single(),
                        airport_code = c.String(),
                        property_category = c.String(),
                        property_currency = c.String(),
                        star_rating = c.Single(),
                        confidence = c.String(),
                        supplier_type = c.String(),
                        location = c.String(),
                        chain_code_id = c.Int(),
                        region_id = c.Int(),
                        highrate = c.String(),
                        lowrate = c.String(),
                        check_in_time = c.String(),
                        check_out_time = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SletohHotel",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        hotel_id = c.Int(),
                        hotel_name = c.String(),
                        street = c.String(),
                        zip = c.String(),
                        region_id = c.Int(nullable: false),
                        region_name = c.String(),
                        region_parent = c.String(),
                        country_name = c.String(),
                        latitude = c.Single(),
                        longitude = c.Single(),
                        url = c.String(),
                        star_rating = c.Single(),
                        thumbnail_url = c.String(),
                        photo_url = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SletohHotel");
            DropTable("dbo.ExpediaHotel");
        }
    }
}
