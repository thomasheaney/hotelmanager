namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImportChangesLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImportChanges",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Provider = c.String(),
                        FieldName = c.String(),
                        OriginalValue = c.String(),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ImportChanges");
        }
    }
}
