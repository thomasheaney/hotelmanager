namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIDToImportChangesLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImportChanges", "item_ID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ImportChanges", "item_ID");
        }
    }
}
