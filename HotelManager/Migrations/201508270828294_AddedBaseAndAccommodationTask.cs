namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBaseAndAccommodationTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccommodationTask",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccommodationID = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(unicode: false, storeType: "text"),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Accommodation", t => t.AccommodationID, cascadeDelete: true)
                .Index(t => t.AccommodationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccommodationTask", "AccommodationID", "dbo.Accommodation");
            DropIndex("dbo.AccommodationTask", new[] { "AccommodationID" });
            DropTable("dbo.AccommodationTask");
        }
    }
}
