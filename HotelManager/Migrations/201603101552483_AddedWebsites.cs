namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWebsites : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Website",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        WebsiteCode = c.String(),
                        WebsiteName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AccommodationWebsite",
                c => new
                    {
                        AccommodationID = c.Int(nullable: false),
                        WebsiteID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccommodationID, t.WebsiteID })
                .ForeignKey("dbo.Accommodation", t => t.AccommodationID, cascadeDelete: true)
                .ForeignKey("dbo.Website", t => t.WebsiteID, cascadeDelete: true)
                .Index(t => t.AccommodationID)
                .Index(t => t.WebsiteID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccommodationWebsite", "WebsiteID", "dbo.Website");
            DropForeignKey("dbo.AccommodationWebsite", "AccommodationID", "dbo.Accommodation");
            DropIndex("dbo.AccommodationWebsite", new[] { "WebsiteID" });
            DropIndex("dbo.AccommodationWebsite", new[] { "AccommodationID" });
            DropTable("dbo.AccommodationWebsite");
            DropTable("dbo.Website");
        }
    }
}
