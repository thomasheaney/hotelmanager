namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedAccommodationProvidersToInt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Accommodation", "ExpediaHotel_ID", "dbo.ExpediaHotel");
            DropForeignKey("dbo.Accommodation", "SletohHotel_ID", "dbo.SletohHotel");
            DropIndex("dbo.Accommodation", new[] { "ExpediaHotel_ID" });
            DropIndex("dbo.Accommodation", new[] { "SletohHotel_ID" });
            AddColumn("dbo.Accommodation", "SletohHotel", c => c.Int(nullable: false));
            AddColumn("dbo.Accommodation", "ExpediaHotel", c => c.Int(nullable: false));
            DropColumn("dbo.Accommodation", "ExpediaHotel_ID");
            DropColumn("dbo.Accommodation", "SletohHotel_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accommodation", "SletohHotel_ID", c => c.Int());
            AddColumn("dbo.Accommodation", "ExpediaHotel_ID", c => c.Int());
            DropColumn("dbo.Accommodation", "ExpediaHotel");
            DropColumn("dbo.Accommodation", "SletohHotel");
            CreateIndex("dbo.Accommodation", "SletohHotel_ID");
            CreateIndex("dbo.Accommodation", "ExpediaHotel_ID");
            AddForeignKey("dbo.Accommodation", "SletohHotel_ID", "dbo.SletohHotel", "ID");
            AddForeignKey("dbo.Accommodation", "ExpediaHotel_ID", "dbo.ExpediaHotel", "ID");
        }
    }
}
