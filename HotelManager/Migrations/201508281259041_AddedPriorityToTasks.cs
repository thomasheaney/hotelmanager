namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPriorityToTasks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccommodationTask", "priority", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccommodationTask", "priority");
        }
    }
}
