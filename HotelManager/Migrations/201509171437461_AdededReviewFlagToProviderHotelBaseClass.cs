namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdededReviewFlagToProviderHotelBaseClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpediaHotel", "FlaggedForReview", c => c.Boolean(nullable: false));
            AddColumn("dbo.SletohHotel", "FlaggedForReview", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SletohHotel", "FlaggedForReview");
            DropColumn("dbo.ExpediaHotel", "FlaggedForReview");
        }
    }
}
