namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplacedProviderHotelsWithGeneric : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HotelProvider",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProviderID = c.Int(nullable: false),
                        ProviderName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.ExpediaHotel", "provider_id", c => c.Int(nullable: false));
            AddColumn("dbo.ExpediaHotel", "hotel_name", c => c.String());
            AddColumn("dbo.ExpediaHotel", "city_id", c => c.Int(nullable: false));
            AddColumn("dbo.ExpediaHotel", "provider_url", c => c.String());
            AddColumn("dbo.ExpediaHotel", "thumbnail_url", c => c.String());
            AddColumn("dbo.ExpediaHotel", "photo_url", c => c.String());
            AddColumn("dbo.SletohHotel", "provider_id", c => c.Int(nullable: false));
            AddColumn("dbo.SletohHotel", "name", c => c.String());
            AddColumn("dbo.SletohHotel", "address_1", c => c.String());
            AddColumn("dbo.SletohHotel", "address_2", c => c.String());
            AddColumn("dbo.SletohHotel", "state_province", c => c.String());
            AddColumn("dbo.SletohHotel", "postal_code", c => c.String());
            AddColumn("dbo.SletohHotel", "country", c => c.String());
            AddColumn("dbo.SletohHotel", "city_id", c => c.Int(nullable: false));
            AddColumn("dbo.SletohHotel", "airport_code", c => c.String());
            AddColumn("dbo.SletohHotel", "provider_url", c => c.String());
            AddColumn("dbo.SletohHotel", "chain_code_id", c => c.Int());
            AddColumn("dbo.SletohHotel", "highrate", c => c.String());
            AddColumn("dbo.SletohHotel", "lowrate", c => c.String());
            AddColumn("dbo.SletohHotel", "check_in_time", c => c.String());
            AddColumn("dbo.SletohHotel", "check_out_time", c => c.String());
            DropColumn("dbo.ExpediaHotel", "hotel_id");
            DropColumn("dbo.ExpediaHotel", "sequence_number");
            DropColumn("dbo.ExpediaHotel", "city");
            DropColumn("dbo.ExpediaHotel", "property_category");
            DropColumn("dbo.ExpediaHotel", "property_currency");
            DropColumn("dbo.ExpediaHotel", "confidence");
            DropColumn("dbo.ExpediaHotel", "supplier_type");
            DropColumn("dbo.ExpediaHotel", "location");
            DropColumn("dbo.ExpediaHotel", "region_id");
            DropColumn("dbo.SletohHotel", "hotel_id");
            DropColumn("dbo.SletohHotel", "street");
            DropColumn("dbo.SletohHotel", "zip");
            DropColumn("dbo.SletohHotel", "region_id");
            DropColumn("dbo.SletohHotel", "region_name");
            DropColumn("dbo.SletohHotel", "region_parent");
            DropColumn("dbo.SletohHotel", "country_name");
            DropColumn("dbo.SletohHotel", "url");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SletohHotel", "url", c => c.String());
            AddColumn("dbo.SletohHotel", "country_name", c => c.String());
            AddColumn("dbo.SletohHotel", "region_parent", c => c.String());
            AddColumn("dbo.SletohHotel", "region_name", c => c.String());
            AddColumn("dbo.SletohHotel", "region_id", c => c.Int(nullable: false));
            AddColumn("dbo.SletohHotel", "zip", c => c.String());
            AddColumn("dbo.SletohHotel", "street", c => c.String());
            AddColumn("dbo.SletohHotel", "hotel_id", c => c.Int());
            AddColumn("dbo.ExpediaHotel", "region_id", c => c.Int());
            AddColumn("dbo.ExpediaHotel", "location", c => c.String());
            AddColumn("dbo.ExpediaHotel", "supplier_type", c => c.String());
            AddColumn("dbo.ExpediaHotel", "confidence", c => c.String());
            AddColumn("dbo.ExpediaHotel", "property_currency", c => c.String());
            AddColumn("dbo.ExpediaHotel", "property_category", c => c.String());
            AddColumn("dbo.ExpediaHotel", "city", c => c.String());
            AddColumn("dbo.ExpediaHotel", "sequence_number", c => c.Int());
            AddColumn("dbo.ExpediaHotel", "hotel_id", c => c.Int());
            DropColumn("dbo.SletohHotel", "check_out_time");
            DropColumn("dbo.SletohHotel", "check_in_time");
            DropColumn("dbo.SletohHotel", "lowrate");
            DropColumn("dbo.SletohHotel", "highrate");
            DropColumn("dbo.SletohHotel", "chain_code_id");
            DropColumn("dbo.SletohHotel", "provider_url");
            DropColumn("dbo.SletohHotel", "airport_code");
            DropColumn("dbo.SletohHotel", "city_id");
            DropColumn("dbo.SletohHotel", "country");
            DropColumn("dbo.SletohHotel", "postal_code");
            DropColumn("dbo.SletohHotel", "state_province");
            DropColumn("dbo.SletohHotel", "address_2");
            DropColumn("dbo.SletohHotel", "address_1");
            DropColumn("dbo.SletohHotel", "name");
            DropColumn("dbo.SletohHotel", "provider_id");
            DropColumn("dbo.ExpediaHotel", "photo_url");
            DropColumn("dbo.ExpediaHotel", "thumbnail_url");
            DropColumn("dbo.ExpediaHotel", "provider_url");
            DropColumn("dbo.ExpediaHotel", "city_id");
            DropColumn("dbo.ExpediaHotel", "hotel_name");
            DropColumn("dbo.ExpediaHotel", "provider_id");
            DropTable("dbo.HotelProvider");
        }
    }
}
