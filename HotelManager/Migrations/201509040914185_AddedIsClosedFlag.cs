namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsClosedFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accommodation", "IsClosed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Accommodation", "redirectToID", c => c.Int(nullable: false));
            AddColumn("dbo.ExpediaHotel", "IsClosed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExpediaHotel", "redirectToID", c => c.Int(nullable: false));
            AddColumn("dbo.SletohHotel", "IsClosed", c => c.Boolean(nullable: false));
            AddColumn("dbo.SletohHotel", "redirectToID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SletohHotel", "redirectToID");
            DropColumn("dbo.SletohHotel", "IsClosed");
            DropColumn("dbo.ExpediaHotel", "redirectToID");
            DropColumn("dbo.ExpediaHotel", "IsClosed");
            DropColumn("dbo.Accommodation", "redirectToID");
            DropColumn("dbo.Accommodation", "IsClosed");
        }
    }
}
