namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMasterListFlagToProviderHotel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpediaHotel", "in_master_list", c => c.Boolean(nullable: false));
            AddColumn("dbo.SletohHotel", "in_master_list", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SletohHotel", "in_master_list");
            DropColumn("dbo.ExpediaHotel", "in_master_list");
        }
    }
}
