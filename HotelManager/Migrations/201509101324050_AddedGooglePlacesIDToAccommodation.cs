namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGooglePlacesIDToAccommodation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accommodation", "GooglePlacesID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accommodation", "GooglePlacesID");
        }
    }
}
