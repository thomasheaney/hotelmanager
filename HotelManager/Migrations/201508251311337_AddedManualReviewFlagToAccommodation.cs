namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedManualReviewFlagToAccommodation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accommodation", "flag_for_review", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accommodation", "flag_for_review");
        }
    }
}
