namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProviderReferenceToAccommodation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accommodation", "ExpediaHotel_ID", c => c.Int());
            AddColumn("dbo.Accommodation", "SletohHotel_ID", c => c.Int());
            CreateIndex("dbo.Accommodation", "ExpediaHotel_ID");
            CreateIndex("dbo.Accommodation", "SletohHotel_ID");
            AddForeignKey("dbo.Accommodation", "ExpediaHotel_ID", "dbo.ExpediaHotel", "ID");
            AddForeignKey("dbo.Accommodation", "SletohHotel_ID", "dbo.SletohHotel", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accommodation", "SletohHotel_ID", "dbo.SletohHotel");
            DropForeignKey("dbo.Accommodation", "ExpediaHotel_ID", "dbo.ExpediaHotel");
            DropIndex("dbo.Accommodation", new[] { "SletohHotel_ID" });
            DropIndex("dbo.Accommodation", new[] { "ExpediaHotel_ID" });
            DropColumn("dbo.Accommodation", "SletohHotel_ID");
            DropColumn("dbo.Accommodation", "ExpediaHotel_ID");
        }
    }
}
