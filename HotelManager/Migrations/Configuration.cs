namespace HotelManager.Migrations
{
    using HotelManager.Helpers;
    using HotelManager.Models;
    using HotelManager.Models.Providers;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HotelManager.DAL.HotelManagerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HotelManager.DAL.HotelManagerContext context)
        {
            bool overrideSeed = false;

            AddUserAndRole(context);
            AddNormalUserAndRole(context);

            var locations = new List<Location>
            {
                new Location { LocationName="Scotland", LocationSearchString = "scotland", Slug = presentationHelper.Slugify("Scotland")},
                new Location { LocationName="Fuerteventura", LocationSearchString = "fuerteventura", Slug = presentationHelper.Slugify("Fuerteventura")}

            };

            //Stop data being added to the table if it is already there
            if (context.Locations.Count() == 0 || overrideSeed)
            {
                locations.ForEach(s => context.Locations.Add(s));
                context.SaveChanges();
            }

            var regions = new List<Region>
           {
                new Region{Slug=presentationHelper.Slugify("Aberdeenshire and Moray"),RegionName="Aberdeenshire and Moray", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Angus and Dundee"),RegionName="Angus and Dundee", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Argyle and Bute"),RegionName="Argyle and Bute", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Ayrshire and Arran"),RegionName="Ayrshire and Arran", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Dumfries and Galloway"),RegionName="Dumfries and Galloway", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Edinburgh and The Lothians"),RegionName="Edinburgh and The Lothians", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Fife"),RegionName="Fife", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Glasgow and the Clyde Valley"),RegionName="Glasgow and the Clyde Valley", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("The Hebrides"),RegionName="The Hebrides", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("The Highlands"),RegionName="The Highlands", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Loch Lomond, Stirling and The Trossachs"),RegionName="Loch Lomond, Stirling and The Trossachs", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Orkney Islands"),RegionName="Orkney Islands", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Perthshire"),RegionName="Perthshire", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Scottish Borders"),RegionName="Scottish Borders", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Shetland Islands"),RegionName="Shetland Islands", RegionSummary="", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Unknown"),RegionName="Unknown", RegionSummary="Holding for towns not in the list.", RegionDescription="",LocationID=1},
                new Region{Slug=presentationHelper.Slugify("Fuertaventura"),RegionName="Fuertaventura", RegionSummary="Fuertaventura", RegionDescription="",LocationID=2}
           };

            //Stop data being added to the table if it is already there
            if (context.Regions.Count() == 0 || overrideSeed)
            {
                regions.ForEach(s => context.Regions.Add(s));
                context.SaveChanges();
            }

            var town = new List<Town>
            {
                new Town{Slug=presentationHelper.Slugify("Unknown"),RegionID=16,TownName="Unknown",TownSummary="",TownDescription="",SletohID=0, Show = true},
                new Town{Slug=presentationHelper.Slugify("Aberdeen"),RegionID=1,TownName="Aberdeen",TownSummary="",TownDescription="",SletohID=1093, Show = true},
                new Town{Slug=presentationHelper.Slugify("Aberdour"),RegionID=7,TownName="Aberdour",TownSummary="",TownDescription="",SletohID=1094, Show = true},
                new Town{Slug=presentationHelper.Slugify("Aberfoyle"),RegionID=11,TownName="Aberfoyle",TownSummary="",TownDescription="",SletohID=1104, Show = true},
                new Town{Slug=presentationHelper.Slugify("Aberlady"),RegionID=6,TownName="Aberlady",TownSummary="",TownDescription="",SletohID=1114, Show = true},
                new Town{Slug=presentationHelper.Slugify("Alyth"),RegionID=13,TownName="Alyth",TownSummary="",TownDescription="",SletohID=1370, Show = true},
                new Town{Slug=presentationHelper.Slugify("Anstruther"),RegionID=7,TownName="Anstruther",TownSummary="",TownDescription="",SletohID=1424, Show = true},
                new Town{Slug=presentationHelper.Slugify("Ardrossan"),RegionID=4,TownName="Ardrossan",TownSummary="",TownDescription="",SletohID=1496, Show = true},
                new Town{Slug=presentationHelper.Slugify("Arisaig"),RegionID=10,TownName="Arisaig",TownSummary="",TownDescription="",SletohID=1515, Show = true},
                new Town{Slug=presentationHelper.Slugify("Aviemore"),RegionID=10,TownName="Aviemore",TownSummary="",TownDescription="",SletohID=1735, Show = true},
                new Town{Slug=presentationHelper.Slugify("Ayr"),RegionID=4,TownName="Ayr",TownSummary="",TownDescription="",SletohID=1761, Show = true},
                new Town{Slug=presentationHelper.Slugify("Ballater"),RegionID=1,TownName="Ballater",TownSummary="",TownDescription="",SletohID=1843, Show = true},
                new Town{Slug=presentationHelper.Slugify("Balloch"),RegionID=8,TownName="Balloch",TownSummary="",TownDescription="",SletohID=1855, Show = true},
                new Town{Slug=presentationHelper.Slugify("Banchory"),RegionID=1,TownName="Banchory",TownSummary="",TownDescription="",SletohID=1938, Show = true},
                new Town{Slug=presentationHelper.Slugify("Banff"),RegionID=1,TownName="Banff",TownSummary="",TownDescription="",SletohID=1941, Show = true},
                new Town{Slug=presentationHelper.Slugify("Biggar"),RegionID=8,TownName="Biggar",TownSummary="",TownDescription="",SletohID=2371, Show = true},
                new Town{Slug=presentationHelper.Slugify("Blackwaterfoot"),RegionID=4,TownName="Blackwaterfoot",TownSummary="",TownDescription="",SletohID=2516, Show = true},
                new Town{Slug=presentationHelper.Slugify("Blair Atholl"),RegionID=13,TownName="Blair Atholl",TownSummary="",TownDescription="",SletohID=2539, Show = true},
                new Town{Slug=presentationHelper.Slugify("Blairgowrie"),RegionID=13,TownName="Blairgowrie",TownSummary="",TownDescription="",SletohID=2540, Show = true},
                new Town{Slug=presentationHelper.Slugify("Bo'ness"),RegionID=11,TownName="Bo'ness",TownSummary="",TownDescription="",SletohID=2599, Show = true},
                new Town{Slug=presentationHelper.Slugify("Bothwell"),RegionID=8,TownName="Bothwell",TownSummary="",TownDescription="",SletohID=2710, Show = true},
                new Town{Slug=presentationHelper.Slugify("Brechin"),RegionID=2,TownName="Brechin",TownSummary="",TownDescription="",SletohID=2913, Show = true},
                new Town{Slug=presentationHelper.Slugify("Bridge of Allan"),RegionID=11,TownName="Bridge of Allan",TownSummary="",TownDescription="",SletohID=2951, Show = true},
                new Town{Slug=presentationHelper.Slugify("Brodick"),RegionID=4,TownName="Brodick",TownSummary="",TownDescription="",SletohID=3067, Show = true},
                new Town{Slug=presentationHelper.Slugify("Buckie"),RegionID=1,TownName="Buckie",TownSummary="",TownDescription="",SletohID=3191, Show = true},
                new Town{Slug=presentationHelper.Slugify("Burntisland"),RegionID=7,TownName="Burntisland",TownSummary="",TownDescription="",SletohID=3285, Show = true},
                new Town{Slug=presentationHelper.Slugify("Callander"),RegionID=11,TownName="Callander",TownSummary="",TownDescription="",SletohID=3428, Show = true},
                new Town{Slug=presentationHelper.Slugify("Campbeltown"),RegionID=3,TownName="Campbeltown",TownSummary="",TownDescription="",SletohID=3471, Show = true},
                new Town{Slug=presentationHelper.Slugify("Carnoustie"),RegionID=2,TownName="Carnoustie",TownSummary="",TownDescription="",SletohID=3590, Show = true},
                new Town{Slug=presentationHelper.Slugify("Carrbridge"),RegionID=10,TownName="Carrbridge",TownSummary="",TownDescription="",SletohID=3596, Show = true},
                new Town{Slug=presentationHelper.Slugify("Castle Douglas"),RegionID=5,TownName="Castle Douglas",TownSummary="",TownDescription="",SletohID=3642, Show = true},
                new Town{Slug=presentationHelper.Slugify("Coatbridge"),RegionID=8,TownName="Coatbridge",TownSummary="",TownDescription="",SletohID=4198, Show = true},
                new Town{Slug=presentationHelper.Slugify("Coldstream"),RegionID=14,TownName="Coldstream",TownSummary="",TownDescription="",SletohID=4260, Show = true},
                new Town{Slug=presentationHelper.Slugify("Craigellachie"),RegionID=1,TownName="Craigellachie",TownSummary="",TownDescription="",SletohID=4554, Show = true},
                new Town{Slug=presentationHelper.Slugify("Cromarty"),RegionID=10,TownName="Cromarty",TownSummary="",TownDescription="",SletohID=4679, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dalkeith"),RegionID=6,TownName="Dalkeith",TownSummary="",TownDescription="",SletohID=4903, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dingwall"),RegionID=10,TownName="Dingwall",TownSummary="",TownDescription="",SletohID=5094, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dollar"),RegionID=11,TownName="Dollar",TownSummary="",TownDescription="",SletohID=5143, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dornoch"),RegionID=10,TownName="Dornoch",TownSummary="",TownDescription="",SletohID=5172, Show = true},
                new Town{Slug=presentationHelper.Slugify("Drumnadrochit"),RegionID=10,TownName="Drumnadrochit",TownSummary="",TownDescription="",SletohID=5272, Show = true},
                new Town{Slug=presentationHelper.Slugify("Drymen"),RegionID=11,TownName="Drymen",TownSummary="",TownDescription="",SletohID=5284, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dumbarton"),RegionID=8,TownName="Dumbarton",TownSummary="",TownDescription="",SletohID=5317, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dumfries"),RegionID=5,TownName="Dumfries",TownSummary="",TownDescription="",SletohID=5319, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dunbar"),RegionID=6,TownName="Dunbar",TownSummary="",TownDescription="",SletohID=5327, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dunblane"),RegionID=11,TownName="Dunblane",TownSummary="",TownDescription="",SletohID=5329, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dundee"),RegionID=2,TownName="Dundee",TownSummary="",TownDescription="",SletohID=5333, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dunfermline"),RegionID=7,TownName="Dunfermline",TownSummary="",TownDescription="",SletohID=5344, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dunkeld"),RegionID=13,TownName="Dunkeld",TownSummary="",TownDescription="",SletohID=5356, Show = true},
                new Town{Slug=presentationHelper.Slugify("Dunoon"),RegionID=3,TownName="Dunoon",TownSummary="",TownDescription="",SletohID=5375, Show = true},
                new Town{Slug=presentationHelper.Slugify("Durness"),RegionID=10,TownName="Durness",TownSummary="",TownDescription="",SletohID=5403, Show = true},
                new Town{Slug=presentationHelper.Slugify("East Kilbride"),RegionID=8,TownName="East Kilbride",TownSummary="",TownDescription="",SletohID=5528, Show = true},
                new Town{Slug=presentationHelper.Slugify("Edinburgh"),RegionID=6,TownName="Edinburgh",TownSummary="",TownDescription="",SletohID=5677, Show = true},
                new Town{Slug=presentationHelper.Slugify("Elgin"),RegionID=1,TownName="Elgin",TownSummary="",TownDescription="",SletohID=5735, Show = true},
                new Town{Slug=presentationHelper.Slugify("Falkirk"),RegionID=11,TownName="Falkirk",TownSummary="",TownDescription="",SletohID=5959, Show = true},
                new Town{Slug=presentationHelper.Slugify("Forfar"),RegionID=2,TownName="Forfar",TownSummary="",TownDescription="",SletohID=6237, Show = true},
                new Town{Slug=presentationHelper.Slugify("Forres"),RegionID=1,TownName="Forres",TownSummary="",TownDescription="",SletohID=6245, Show = true},
                new Town{Slug=presentationHelper.Slugify("Fort Augustus"),RegionID=10,TownName="Fort Augustus",TownSummary="",TownDescription="",SletohID=6250},
                new Town{Slug=presentationHelper.Slugify("Fort William"),RegionID=10,TownName="Fort William",TownSummary="",TownDescription="",SletohID=6252, Show = true},
                new Town{Slug=presentationHelper.Slugify("Gairloch"),RegionID=3,TownName="Gairloch",TownSummary="",TownDescription="",SletohID=6403, Show = true},
                new Town{Slug=presentationHelper.Slugify("Gatehouse of Fleet"),RegionID=5,TownName="Gatehouse of Fleet",TownSummary="",TownDescription="",SletohID=6472, Show = true},
                new Town{Slug=presentationHelper.Slugify("Glasgow"),RegionID=8,TownName="Glasgow",TownSummary="",TownDescription="",SletohID=6564, Show = true},
                new Town{Slug=presentationHelper.Slugify("Glenfinnan"),RegionID=10,TownName="Glenfinnan",TownSummary="",TownDescription="",SletohID=6600, Show = true},
                new Town{Slug=presentationHelper.Slugify("Glenrothes"),RegionID=7,TownName="Glenrothes",TownSummary="",TownDescription="",SletohID=6615, Show = true},
                new Town{Slug=presentationHelper.Slugify("Gourock"),RegionID=8,TownName="Gourock",TownSummary="",TownDescription="",SletohID=6705, Show = true},
                new Town{Slug=presentationHelper.Slugify("Grangemouth"),RegionID=11,TownName="Grangemouth",TownSummary="",TownDescription="",SletohID=6738, Show = true},
                new Town{Slug=presentationHelper.Slugify("Grantown on Spey"),RegionID=10,TownName="Grantown on Spey",TownSummary="",TownDescription="",SletohID=6746, Show = true},
                new Town{Slug=presentationHelper.Slugify("Greenock"),RegionID=8,TownName="Greenock",TownSummary="",TownDescription="",SletohID=6942, Show = true},
                new Town{Slug=presentationHelper.Slugify("Gretna Green"),RegionID=5,TownName="Gretna Green",TownSummary="",TownDescription="",SletohID=6958, Show = true},
                new Town{Slug=presentationHelper.Slugify("Gullane"),RegionID=6,TownName="Gullane",TownSummary="",TownDescription="",SletohID=7019, Show = true},
                new Town{Slug=presentationHelper.Slugify("Hamilton"),RegionID=8,TownName="Hamilton",TownSummary="",TownDescription="",SletohID=7142, Show = true},
                new Town{Slug=presentationHelper.Slugify("Hawick"),RegionID=14,TownName="Hawick",TownSummary="",TownDescription="",SletohID=7338, Show = true},
                new Town{Slug=presentationHelper.Slugify("Huntly"),RegionID=1,TownName="Huntly",TownSummary="",TownDescription="",SletohID=7953, Show = true},
                new Town{Slug=presentationHelper.Slugify("Inveraray"),RegionID=3,TownName="Inveraray",TownSummary="",TownDescription="",SletohID=8100, Show = true},
                new Town{Slug=presentationHelper.Slugify("Inverness"),RegionID=10,TownName="Inverness",TownSummary="",TownDescription="",SletohID=8138, Show = true},
                new Town{Slug=presentationHelper.Slugify("Irvine"),RegionID=4,TownName="Irvine",TownSummary="",TownDescription="",SletohID=8167, Show = true},
                new Town{Slug=presentationHelper.Slugify("Jedburgh"),RegionID=14,TownName="Jedburgh",TownSummary="",TownDescription="",SletohID=8201, Show = true},
                new Town{Slug=presentationHelper.Slugify("Johnstone"),RegionID=8,TownName="Johnstone",TownSummary="",TownDescription="",SletohID=8211, Show = true},
                new Town{Slug=presentationHelper.Slugify("Keith"),RegionID=1,TownName="Keith",TownSummary="",TownDescription="",SletohID=8245, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kelso"),RegionID=14,TownName="Kelso",TownSummary="",TownDescription="",SletohID=8262, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kenmore"),RegionID=13,TownName="Kenmore",TownSummary="",TownDescription="",SletohID=8285, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kilmarnock"),RegionID=4,TownName="Kilmarnock",TownSummary="",TownDescription="",SletohID=8422, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kilsyth"),RegionID=8,TownName="Kilsyth",TownSummary="",TownDescription="",SletohID=8467, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kilwinning"),RegionID=4,TownName="Kilwinning",TownSummary="",TownDescription="",SletohID=8473, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kingussie"),RegionID=10,TownName="Kingussie",TownSummary="",TownDescription="",SletohID=8572, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kinross"),RegionID=13,TownName="Kinross",TownSummary="",TownDescription="",SletohID=8601, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kirkcaldy"),RegionID=7,TownName="Kirkcaldy",TownSummary="",TownDescription="",SletohID=8661, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kirkcudbright"),RegionID=5,TownName="Kirkcudbright",TownSummary="",TownDescription="",SletohID=8668, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kirkton of Glenisla"),RegionID=2,TownName="Kirkton of Glenisla",TownSummary="",TownDescription="",SletohID=8713, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kirriemuir"),RegionID=2,TownName="Kirriemuir",TownSummary="",TownDescription="",SletohID=8732, Show = true},
                new Town{Slug=presentationHelper.Slugify("Kyle of Lochalsh"),RegionID=10,TownName="Kyle of Lochalsh",TownSummary="",TownDescription="",SletohID=8791, Show = true},
                new Town{Slug=presentationHelper.Slugify("Lamlash"),RegionID=4,TownName="Lamlash",TownSummary="",TownDescription="",SletohID=8840, Show = true},
                new Town{Slug=presentationHelper.Slugify("Lanark"),RegionID=8,TownName="Lanark",TownSummary="",TownDescription="",SletohID=8847, Show = true},
                new Town{Slug=presentationHelper.Slugify("Largs"),RegionID=4,TownName="Largs",TownSummary="",TownDescription="",SletohID=8911, Show = true},
                new Town{Slug=presentationHelper.Slugify("Leven"),RegionID=7,TownName="Leven",TownSummary="",TownDescription="",SletohID=9085, Show = true},
                new Town{Slug=presentationHelper.Slugify("Linlithgow"),RegionID=6,TownName="Linlithgow",TownSummary="",TownDescription="",SletohID=9154, Show = true},
                new Town{Slug=presentationHelper.Slugify("Livingston"),RegionID=6,TownName="Livingston",TownSummary="",TownDescription="",SletohID=9328, Show = true},
                new Town{Slug=presentationHelper.Slugify("Lockerbie"),RegionID=5,TownName="Lockerbie",TownSummary="",TownDescription="",SletohID=9681, Show = true},
                new Town{Slug=presentationHelper.Slugify("Lossiemouth"),RegionID=1,TownName="Lossiemouth",TownSummary="",TownDescription="",SletohID=9806, Show = true},
                new Town{Slug=presentationHelper.Slugify("Luss"),RegionID=3,TownName="Luss",TownSummary="",TownDescription="",SletohID=9948, Show = true},
                new Town{Slug=presentationHelper.Slugify("Macduff"),RegionID=1,TownName="Macduff",TownSummary="",TownDescription="",SletohID=10001, Show = true},
                new Town{Slug=presentationHelper.Slugify("Mallaig"),RegionID=10,TownName="Mallaig",TownSummary="",TownDescription="",SletohID=10065, Show = true},
                new Town{Slug=presentationHelper.Slugify("Melrose"),RegionID=14,TownName="Melrose",TownSummary="",TownDescription="",SletohID=10307, Show = true},
                new Town{Slug=presentationHelper.Slugify("Montrose"),RegionID=2,TownName="Montrose",TownSummary="",TownDescription="",SletohID=10598, Show = true},
                new Town{Slug=presentationHelper.Slugify("Motherwell"),RegionID=8,TownName="Motherwell",TownSummary="",TownDescription="",SletohID=10674, Show = true},
                new Town{Slug=presentationHelper.Slugify("Musselburgh"),RegionID=6,TownName="Musselburgh",TownSummary="",TownDescription="",SletohID=10757, Show = true},
                new Town{Slug=presentationHelper.Slugify("Newburgh"),RegionID=7,TownName="Newburgh",TownSummary="",TownDescription="",SletohID=10943, Show = true},
                new Town{Slug=presentationHelper.Slugify("Newton Stewart"),RegionID=5,TownName="Newton Stewart",TownSummary="",TownDescription="",SletohID=11042, Show = true},
                new Town{Slug=presentationHelper.Slugify("North Berwick"),RegionID=6,TownName="North Berwick",TownSummary="",TownDescription="",SletohID=11100, Show = true},
                new Town{Slug=presentationHelper.Slugify("Oban"),RegionID=3,TownName="Oban",TownSummary="",TownDescription="",SletohID=11296, Show = true},
                new Town{Slug=presentationHelper.Slugify("Paisley"),RegionID=8,TownName="Paisley",TownSummary="",TownDescription="",SletohID=11518, Show = true},
                new Town{Slug=presentationHelper.Slugify("Peebles"),RegionID=14,TownName="Peebles",TownSummary="",TownDescription="",SletohID=11611, Show = true},
                new Town{Slug=presentationHelper.Slugify("Perth"),RegionID=13,TownName="Perth",TownSummary="",TownDescription="",SletohID=11727, Show = true},
                new Town{Slug=presentationHelper.Slugify("Peterhead"),RegionID=1,TownName="Peterhead",TownSummary="",TownDescription="",SletohID=11732, Show = true},
                new Town{Slug=presentationHelper.Slugify("Pitlochry"),RegionID=13,TownName="Pitlochry",TownSummary="",TownDescription="",SletohID=11798, Show = true},
                new Town{Slug=presentationHelper.Slugify("Polmont"),RegionID=11,TownName="Polmont",TownSummary="",TownDescription="",SletohID=11863, Show = true},
                new Town{Slug=presentationHelper.Slugify("Portpatrick"),RegionID=5,TownName="Portpatrick",TownSummary="",TownDescription="",SletohID=11989, Show = true},
                new Town{Slug=presentationHelper.Slugify("Portree"),RegionID=9,TownName="Portree",TownSummary="",TownDescription="",SletohID=11991, Show = true},
                new Town{Slug=presentationHelper.Slugify("Prestwick"),RegionID=4,TownName="Prestwick",TownSummary="",TownDescription="",SletohID=12066, Show = true},
                new Town{Slug=presentationHelper.Slugify("Seamill"),RegionID=4,TownName="Seamill",TownSummary="",TownDescription="",SletohID=13010, Show = true},
                new Town{Slug=presentationHelper.Slugify("Selkirk"),RegionID=14,TownName="Selkirk",TownSummary="",TownDescription="",SletohID=13045, Show = true},
                new Town{Slug=presentationHelper.Slugify("St Andrews"),RegionID=7,TownName="St Andrews",TownSummary="",TownDescription="",SletohID=13650, Show = true},
                new Town{Slug=presentationHelper.Slugify("Stirling"),RegionID=11,TownName="Stirling",TownSummary="",TownDescription="",SletohID=13828, Show = true},
                new Town{Slug=presentationHelper.Slugify("Stranraer"),RegionID=5,TownName="Stranraer",TownSummary="",TownDescription="",SletohID=13977, Show = true},
                new Town{Slug=presentationHelper.Slugify("Strathpeffer"),RegionID=10,TownName="Strathpeffer",TownSummary="",TownDescription="",SletohID=13991, Show = true},
                new Town{Slug=presentationHelper.Slugify("Tain"),RegionID=10,TownName="Tain",TownSummary="",TownDescription="",SletohID=14214, Show = true},
                new Town{Slug=presentationHelper.Slugify("Tarbert"),RegionID=3,TownName="Tarbert",TownSummary="",TownDescription="",SletohID=14255, Show = true},
                new Town{Slug=presentationHelper.Slugify("Tarbet"),RegionID=3,TownName="Tarbet",TownSummary="",TownDescription="",SletohID=14259, Show = true},
                new Town{Slug=presentationHelper.Slugify("Taynuilt"),RegionID=3,TownName="Taynuilt",TownSummary="",TownDescription="",SletohID=14296, Show = true},
                new Town{Slug=presentationHelper.Slugify("Thurso"),RegionID=10,TownName="Thurso",TownSummary="",TownDescription="",SletohID=14504, Show = true},
                new Town{Slug=presentationHelper.Slugify("Tobermory"),RegionID=9,TownName="Tobermory",TownSummary="",TownDescription="",SletohID=14590, Show = true},
                new Town{Slug=presentationHelper.Slugify("Tyndrum"),RegionID=11,TownName="Tyndrum",TownSummary="",TownDescription="",SletohID=14866, Show = true},
                new Town{Slug=presentationHelper.Slugify("Uddingston"),RegionID=8,TownName="Uddingston",TownSummary="",TownDescription="",SletohID=14882, Show = true},
                new Town{Slug=presentationHelper.Slugify("Ullapool"),RegionID=10,TownName="Ullapool",TownSummary="",TownDescription="",SletohID=14906, Show = true},
                new Town{Slug=presentationHelper.Slugify("Wick"),RegionID=10,TownName="Wick",TownSummary="",TownDescription="",SletohID=15738, Show = true},
                new Town{ SletohID=162671, TownName="Antigua", Slug=presentationHelper.Slugify("Antigua"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=165742, TownName="Caleta de Fuste", Slug=presentationHelper.Slugify("Caleta de Fuste"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=168289, TownName="Corralejo", Slug=presentationHelper.Slugify("Corralejo"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=168438, TownName="Costa Calma", Slug=presentationHelper.Slugify("Costa Calma"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=419778, TownName="Cotillo", Slug=presentationHelper.Slugify("Cotillo"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=419912, TownName="El Cotillo", Slug=presentationHelper.Slugify("El Cotillo"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=170558, TownName="Esquinzo", Slug=presentationHelper.Slugify("Esquinzo"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=173191, TownName="Jandia", Slug=presentationHelper.Slugify("Jandia"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=174234, TownName="La Oliva", Slug=presentationHelper.Slugify("La Oliva"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=419968, TownName="Las Playitas", Slug=presentationHelper.Slugify("Las Playitas"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=175342, TownName="Las Salinas", Slug=presentationHelper.Slugify("Las Salinas"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=178414, TownName="Morro del Jable", Slug=presentationHelper.Slugify("Morro del Jable"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=186957, TownName="Nuevo Horizonte", Slug=presentationHelper.Slugify("Nuevo Horizonte"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=181577, TownName="Puerto de la Pena", Slug=presentationHelper.Slugify("Puerto de la Pena"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=181586, TownName="Puerto del Rosario", Slug=presentationHelper.Slugify("Puerto del Rosario"),RegionID=17,TownSummary="",TownDescription="", Show = true},
                new Town{ SletohID=185838, TownName="Tarajalejo", Slug=presentationHelper.Slugify("Tarajalejo"),RegionID=17,TownSummary="",TownDescription="", Show = true}
            };

            //Stop data being added to the table if it is already there
            if (context.Towns.Count() == 0 || overrideSeed) 
            { 
                town.ForEach(s => context.Towns.Add(s));
                context.SaveChanges();
            }

            var provider = new List<HotelProvider>
            {
                new HotelProvider{ProviderID =1, ProviderName="Sletoh"},
                new HotelProvider{ProviderID =2, ProviderName="Expedia"}
            };

            //Stop data being added to the table if it is already there
            if (context.Providers.Count() == 0 || overrideSeed)
            {
                provider.ForEach(s => context.Providers.Add(s));
                context.SaveChanges();
            }

            var facilities = new List<Facility>
           {

                new Facility{FacilityName="Work Desk"},
                new Facility{FacilityName="Coffee and Tea Making Facilities"},
                new Facility{FacilityName="Iron and Ironing Board"},
                new Facility{FacilityName="Complementary High Speed Wireless Internet access"},
                new Facility{FacilityName="Deluxe Toiletries"},
                new Facility{FacilityName="Direct Dial Telephone"},
                new Facility{FacilityName="Flat Screen TV"},
                new Facility{FacilityName="Hairdryer"}
                
           };

            //Stop data being added to the table if it is already there
            if (context.Facilities.Count() == 0 || overrideSeed)
            {
                facilities.ForEach(s => context.Facilities.Add(s));
                context.SaveChanges();
            }



            var websites = new List<Website>
           {

                new Website{WebsiteCode="ONS", WebsiteName="ONS"},
                new Website{WebsiteCode="SBS", WebsiteName="SBS"},
                new Website{WebsiteCode="SWR", WebsiteName="SWR"},
                new Website{WebsiteCode="CJH", WebsiteName="CJH"}
                
           };

            //Stop data being added to the table if it is already there
            if (context.Websites.Count() == 0 || overrideSeed)
            {
                websites.ForEach(s => context.Websites.Add(s));
                context.SaveChanges();
            }


            if (context.Accommodations.Count() > 0 || overrideSeed)
            {

                AddOrUpdateFacility(context, 19, 1);
                AddOrUpdateFacility(context, 19, 2);
                AddOrUpdateFacility(context, 19, 3);
            }
        }

        

        void AddOrUpdateFacility(HotelManager.DAL.HotelManagerContext context, int accommodationID, int facilityID)
        {
            var accommodation = context.Accommodations.SingleOrDefault(a => a.ID == accommodationID);
            if (accommodation.Facilities == null)
            {
                accommodation.Facilities = new List<Facility>();
            }
            var facility = accommodation.Facilities.SingleOrDefault(f => f.ID == facilityID);
            if (facility == null)
                accommodation.Facilities.Add(context.Facilities.Single(f => f.ID == facilityID));
        }

        bool AddUserAndRole(HotelManager.DAL.HotelManagerContext context)
        {
            IdentityResult ir;
            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            ir = rm.Create(new IdentityRole("canEdit"));
            ir = rm.Create(new IdentityRole("canEditAll"));
            ir = rm.Create(new IdentityRole("canViewAll"));
            ir = rm.Create(new IdentityRole("canImport"));
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var user = new ApplicationUser()
            {
                UserName = "admin@hotelmanager.com",
            };

            ir = um.Create(user, "P_assw0rd1");

            if (ir.Succeeded == false)
                return ir.Succeeded;

            ir = um.AddToRole(user.Id, "canEdit");
            ir = um.AddToRole(user.Id, "canEditAll");
            ir = um.AddToRole(user.Id, "canViewAll");
            ir = um.AddToRole(user.Id, "canImport");
            return ir.Succeeded;
        }

        bool AddNormalUserAndRole(HotelManager.DAL.HotelManagerContext context)
        {
            IdentityResult ir;
            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            ir = rm.Create(new IdentityRole("canEdit"));
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var user = new ApplicationUser()
            {
                UserName = "test@test.com",
            };

            ir = um.Create(user, "P_assw0rd1");

            if (ir.Succeeded == false)
                return ir.Succeeded;

            ir = um.AddToRole(user.Id, "canEdit");

            return ir.Succeeded;
        }
    }
}
