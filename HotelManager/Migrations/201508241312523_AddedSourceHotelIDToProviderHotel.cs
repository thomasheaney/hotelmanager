namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSourceHotelIDToProviderHotel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpediaHotel", "hotel_id", c => c.Int(nullable: false));
            AddColumn("dbo.SletohHotel", "hotel_id", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SletohHotel", "hotel_id");
            DropColumn("dbo.ExpediaHotel", "hotel_id");
        }
    }
}
