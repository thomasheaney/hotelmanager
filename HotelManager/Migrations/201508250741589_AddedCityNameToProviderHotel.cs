namespace HotelManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCityNameToProviderHotel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpediaHotel", "city_name", c => c.String());
            AddColumn("dbo.SletohHotel", "city_name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SletohHotel", "city_name");
            DropColumn("dbo.ExpediaHotel", "city_name");
        }
    }
}
