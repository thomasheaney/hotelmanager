// <auto-generated />
namespace HotelManager.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedAccommodationFacilitiesRelationship : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedAccommodationFacilitiesRelationship));
        
        string IMigrationMetadata.Id
        {
            get { return "201509101453531_AddedAccommodationFacilitiesRelationship"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
