﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelManager.DAL;
using HotelManager.Models.Tasks;
using HotelManager.Models;

namespace HotelManager.Controllers.Tasks
{
    public class AccommodationTasksController : Controller
    {
        private HotelManagerContext db = new HotelManagerContext();

        // GET: AccommodationTasks
        public ActionResult Index()
        {
            var accommodationTasks = db.AccommodationTasks.Include(a => a.Accommodation);
            return View(accommodationTasks.ToList());
        }

        // GET: AccommodationTasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccommodationTask accommodationTask = db.AccommodationTasks.Find(id);
            if (accommodationTask == null)
            {
                return HttpNotFound();
            }
            return View(accommodationTask);
        }

        // GET: AccommodationTasks/Create
        public ActionResult Create(int? accommodationID)
        {
            AccommodationTask task = new AccommodationTask();
            task.priority = 2;
            if (accommodationID != null)
            {
                Accommodation accommodation = db.Accommodations.SingleOrDefault(h => h.ID == accommodationID);
                ViewBag.AcommodationID = accommodation.ID;
                task.AccommodationID = accommodation.ID;
            }
            else
            {
                task.AccommodationID = 0;
            }

            return View(task);
        }

        // POST: AccommodationTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,AccommodationID,Title,Description,status,priority")] AccommodationTask accommodationTask)
        {
            if (ModelState.IsValid)
            {
                db.AccommodationTasks.Add(accommodationTask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccommodationID = new SelectList(db.Accommodations, "ID", "Slug", accommodationTask.AccommodationID);
            return View(accommodationTask);
        }

        // GET: AccommodationTasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccommodationTask accommodationTask = db.AccommodationTasks.Find(id);
            if (accommodationTask == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccommodationID = new SelectList(db.Accommodations, "ID", "Slug", accommodationTask.AccommodationID);
            return View(accommodationTask);
        }

        // POST: AccommodationTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,AccommodationID,Title,Description,status")] AccommodationTask accommodationTask)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accommodationTask).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccommodationID = new SelectList(db.Accommodations, "ID", "Slug", accommodationTask.AccommodationID);
            return View(accommodationTask);
        }

        // GET: AccommodationTasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccommodationTask accommodationTask = db.AccommodationTasks.Find(id);
            if (accommodationTask == null)
            {
                return HttpNotFound();
            }
            return View(accommodationTask);
        }

        // POST: AccommodationTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AccommodationTask accommodationTask = db.AccommodationTasks.Find(id);
            db.AccommodationTasks.Remove(accommodationTask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
