﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelManager.DAL;
using HotelManager.Models;
using HotelManager.ViewModels;
using HotelManager.Models.Providers;
using HotelManager.Helpers;

namespace HotelManager.Controllers
{
    public class AccommodationsController : Controller
    {
        private HotelManagerContext db = new HotelManagerContext();

        // GET: Accommodations
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Accommodations/Details/5
        [AllowAnonymous]
        public ActionResult Details(string slug)
        {
            if (slug == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Accommodation accommodation = db.Accommodations.SingleOrDefault(p => p.PageSlug.ToLower().Equals(slug.ToLower()));
            if (accommodation == null)
            {
                return HttpNotFound();
            }

            SletohHotel SletohHotel = db.SletoHotels.SingleOrDefault(p => p.ProviderHotelID.Equals(accommodation.SletohHotel));
            ExpediaHotel ExpediaHotel = db.ExpediaHotels.SingleOrDefault(p => p.ProviderHotelID.Equals(accommodation.ExpediaHotel));

            vmAccommodationDetail viewModel = new vmAccommodationDetail() 
            { 
                Accommodation = accommodation,
                SletohHotel = SletohHotel,
                ExpediaHotel = ExpediaHotel
            };

            return View("~/Views/Accommodations/Details.cshtml", viewModel);
        }

        // GET: Accommodations/Create
        [Authorize(Roles = "canEdit")]
        public ActionResult Create()
        {
            ViewBag.TownID = new SelectList(db.Towns, "ID", "Slug");
            return View();
        }

        // POST: Accommodations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "canEdit")]
        public ActionResult Create([Bind(Include = "ID,PageSlug,AccommodationName,AddressLine1,AddressLine2,TownName,PostalCode,Latitude,Longitude,StarRating,AccommodationSummary,AccommodationDescription,NearestAirportCode,ParentChainID,HighRate,LowRate,CheckInTime,CheckOutTime,SletohUrl,ExpediaUrl,Published,FlaggedForReview,IsClosed,redirectToID,TownID,SletohHotel,ExpediaHotel,AccommodationWebsite, AccommodationFacebookLink, AccommodationTwitterLink, AccommodationContactEmail,GooglePlacesID")] Accommodation accommodation)
        {
            if (ModelState.IsValid)
            {
                db.Accommodations.Add(accommodation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TownID = new SelectList(db.Towns, "ID", "Slug", accommodation.TownID);
            return View(accommodation);
        }

        // GET: Accommodations/Edit/5
        [Authorize(Roles = "canEdit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accommodation accommodation = db.Accommodations
                .Include(i => i.Facilities).Where(i=> i.ID == id)
                .Include(w => w.Websites).Where(w => w.ID == id)
                .Single();
            PopulateFacilities(accommodation);
            PopulateWebsites(accommodation);

            if (accommodation == null)
            {
                return HttpNotFound();
            }
            ViewBag.TownID = new SelectList(db.Towns, "ID", "Slug", accommodation.TownID);
            return View(accommodation);
        }

        // POST: Accommodations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "canEdit")]
        public ActionResult Edit([Bind(Include = "ID,PageSlug,AccommodationName,AddressLine1,AddressLine2,TownName,PostalCode,Latitude,Longitude,StarRating,AccommodationSummary,AccommodationDescription,NearestAirportCode,ParentChainID,HighRate,LowRate,CheckInTime,CheckOutTime,SletohUrl,ExpediaUrl,Published,FlaggedForReview,IsClosed,redirectToID,TownID,SletohHotel,ExpediaHotel,AccommodationWebsite, AccommodationFacebookLink, AccommodationTwitterLink, AccommodationContactEmail,GooglePlacesID")] Accommodation accommodation, string[] selectedFacilities, string[] selectedWebsites)
        {
             var accommodationToUpdate = db.Accommodations
                    .Include(a => a.Facilities).Where(a => a.ID == accommodation.ID)
                    .Include(w => w.Websites).Where(w => w.ID == accommodation.ID)
                    .Single();
            if (ModelState.IsValid)
            {

                UpdateFacilities(selectedFacilities, accommodationToUpdate);
                UpdateWebsites(selectedWebsites, accommodationToUpdate);

                //db.Entry(accommodation).State = EntityState.Modified;
                db.SaveChanges();
                // If saved return to the detail page of the hotel.
                return RedirectToAction("Details", "Accommodations", new { slug = presentationHelper.Slugify(accommodation.PageSlug) });
            }
            ViewBag.TownID = new SelectList(db.Towns, "ID", "Slug", accommodation.TownID);
            PopulateFacilities(accommodationToUpdate);
            PopulateWebsites(accommodationToUpdate);
            return View(accommodation);
        }

        // GET: Accommodations/Delete/5
        [Authorize(Roles = "canEdit")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accommodation accommodation = db.Accommodations.Find(id);
            if (accommodation == null)
            {
                return HttpNotFound();
            }
            return View(accommodation);
        }

        // POST: Accommodations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "canEdit")]
        public ActionResult DeleteConfirmed(int id)
        {
            Accommodation accommodation = db.Accommodations.Find(id);
            db.Accommodations.Remove(accommodation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void PopulateFacilities(Accommodation accommodation)
        {
            var allFacilities = db.Facilities;
            var accommodationFacilities = new HashSet<int>(accommodation.Facilities.Select(c => c.ID));
            var viewModel = new List<vmAssignedFacility>();

            foreach (var facility in allFacilities)
            {
                viewModel.Add(new vmAssignedFacility
                {
                    FacilityID = facility.ID,
                    FacilityName = facility.FacilityName,
                    Assigned = accommodationFacilities.Contains(facility.ID)
                });
            }

            ViewBag.Facilities = viewModel;
        }

        private void UpdateFacilities(string[] selectedFacilities, Accommodation accommodationToUpdate)
        {
            if (selectedFacilities == null)
            {
                accommodationToUpdate.Facilities = new List<Facility>();
                return;
            }

            var selectedFacilitiesHS = new HashSet<string>(selectedFacilities);
            var accommodationFacilities = new HashSet<int>(accommodationToUpdate.Facilities.Select(f => f.ID));

            foreach (var facility in db.Facilities)
            {
                if (selectedFacilitiesHS.Contains(facility.ID.ToString()))
                {
                    if (!accommodationFacilities.Contains(facility.ID))
                    {
                        accommodationToUpdate.Facilities.Add(facility);
                    }
                }
                else
                {
                    if (accommodationFacilities.Contains(facility.ID))
                    {
                        accommodationToUpdate.Facilities.Remove(facility);
                    }
                }
            }
        }

        private void PopulateWebsites(Accommodation accommodation)
        {
            var allWebsites = db.Websites;
            var accommodationWebsites = new HashSet<int>(accommodation.Websites.Select(c => c.ID));
            var viewModel = new List<vmAssignedWebsite>();

            foreach (var website in allWebsites)
            {
                viewModel.Add(new vmAssignedWebsite
                {
                    WebsiteID = website.ID,
                    WebsiteName = website.WebsiteName,
                    Assigned =  accommodationWebsites.Contains(website.ID)
                });
            }

            ViewBag.Websites = viewModel;
        }

        private void UpdateWebsites(string[] selectedWebsites, Accommodation accommodationToUpdate)
        {
            if (selectedWebsites == null)
            {
                accommodationToUpdate.Websites = new List<Website>();
                return;
            }

            var selectedWebsitesHS = new HashSet<string>(selectedWebsites);
            var accommodationWebsites = new HashSet<int>(accommodationToUpdate.Websites.Select(f => f.ID));

            foreach (var website in db.Websites)
            {
                if (selectedWebsitesHS.Contains(website.ID.ToString()))
                {
                    if (!accommodationWebsites.Contains(website.ID))
                    {
                        accommodationToUpdate.Websites.Add(website);
                    }
                }
                else
                {
                    if (accommodationWebsites.Contains(website.ID))
                    {
                        accommodationToUpdate.Websites.Remove(website);
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}