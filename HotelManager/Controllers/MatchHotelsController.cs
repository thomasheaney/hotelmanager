﻿using HotelManager.DAL;
using HotelManager.Helpers;
using HotelManager.Models;
using HotelManager.Models.Logging;
using HotelManager.Models.Providers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotelManager.Controllers
{
    public class MatchHotelsController : Controller
    {

        private HotelManagerContext context = new HotelManagerContext();

        // GET: MatchHotels
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "canImport")]
        public ActionResult Initialise()
        {
            // Use the initial Sletoh list as a seed.

            //HotelManagerContext context = new DAL.HotelManagerContext();
            List<Accommodation> masterHotels = new List<Accommodation>();

            // Create local lists of entities  
            List<Town> towns = new List<Town>();
            List<Accommodation> accommodation = new List<Accommodation>();

            foreach (var item in context.Towns)
            {
                towns.Add(item);
            }

            foreach (var item in context.Accommodations)
            {
                accommodation.Add(item);
            }


            //Go through each sletoh hotel and add to master list
            foreach (var item in context.SletoHotels)
            {
                // Get city DB key
                var tempTown = towns.Where(i => i.SletohID == item.TownID).FirstOrDefault();

                // Check if this hotel already exists
                var accommExists = accommodation.Where(a => a.SletohHotel == item.ProviderHotelID).Count() > 0 ? true : false;

                // only insert if the hotel doesn't have an entry in the Accommodation table
                if (!accommExists)
                {
                    // Check for existing page slug, if exists then add postcode to slug
                    var slug = presentationHelper.Slugify(item.HotelName + "-" + item.TownName);
                    var slugExists = 
                        accommodation.Where(a => a.PageSlug == slug).Count() > 0
                        || masterHotels.Where(a => a.PageSlug == slug).Count() > 0 ? true : false;

                    if (slugExists) { 
                        slug = slug + "-" + presentationHelper.Slugify(item.PostalCode); 
                    }

                    Accommodation hotel = new Accommodation()
                    {
                        AccommodationName = item.HotelName,
                        AddressLine1 = item.AddressLine1,
                        AddressLine2 = item.AddressLine2,
                        PostalCode = item.PostalCode,
                        TownID = tempTown.ID,
                        SletohHotel = item.ProviderHotelID,
                        SletohUrl = item.ProviderUrl,
                        PageSlug = slug,
                        StarRating = item.StarRating
                    };

                    masterHotels.Add(hotel);
                    item.InMasterList = true;
                }
                
            }

            masterHotels.ForEach(s => context.Accommodations.Add(s));
            context.SaveChanges();

            return View(masterHotels);
        }

        [Authorize(Roles = "canImport")]
        public ActionResult MatchExpedia()
        {
            //HotelManager.DAL.HotelManagerContext context = new DAL.HotelManagerContext();


            List<ExpediaHotel> expediaHotels = new List<ExpediaHotel>();
            List<ExpediaHotel> matchedHotels = new List<ExpediaHotel>();

            List<Accommodation> masterHotels = new List<Accommodation>();

            foreach (var item in context.ExpediaHotels)
            {
                expediaHotels.Add(item);
            }

            // go through each item in the master hotels list and check if there
            // is a matching expedia hotel.
            foreach (Accommodation accommodation in context.Accommodations)
            {

                foreach (ExpediaHotel expediaHotel in expediaHotels)
                {
                    var fullMatch = accommodation.PostalCode == expediaHotel.PostalCode && accommodation.AccommodationName == expediaHotel.HotelName ? true : false;
                    var addressMatch = fullMatch == false
                        && (accommodation.PostalCode == expediaHotel.PostalCode && accommodation.AddressLine1 == expediaHotel.AddressLine1) 
                        ? true : false;


                    if (fullMatch || addressMatch)
                    {
                        // Mark the expedia record as being in the master list
                        ExpediaHotel matchedExpediaHotel = context.ExpediaHotels.Find(expediaHotel.ID);
                        matchedExpediaHotel.InMasterList = true;
                        context.Entry(matchedExpediaHotel).State = EntityState.Modified;

                        // Update the master list expedia hotel refernce field
                        accommodation.ExpediaHotel = expediaHotel.ProviderHotelID;
                        accommodation.Latitude = expediaHotel.Latitude;
                        accommodation.Longitude = expediaHotel.Longitude;
                        accommodation.NearestAirportCode = expediaHotel.NearestAirportCode;
                        accommodation.HighRate = expediaHotel.HighRate;
                        accommodation.LowRate = expediaHotel.LowRate;
                        accommodation.CheckInTime = expediaHotel.CheckInTime;
                        accommodation.CheckOutTime = expediaHotel.CheckOutTime;

                        // An address match should be flagged for a manual check.
                        // Possibly automatically add a task?
                        if (addressMatch)
                        { accommodation.FlaggedForReview = true; }

                        context.Entry(accommodation).State = EntityState.Modified;

                        // Log changes
                        ImportChanges changeLog = new ImportChanges
                        {
                            Provider = "2",
                            item_ID = 0,
                            FieldName = "ExpediaHotel",
                            OriginalValue = "",
                            NewValue = expediaHotel.ProviderHotelID.ToString()

                        };
                        context.ImportChanges.Add(changeLog);

                        matchedHotels.Add(expediaHotel);
                    }
                    else
                    {
                        // Add the expedia hotel as a new entry
                    }
                }

                

            }
            context.SaveChanges();
            return View(matchedHotels);
        }
    }
}