﻿using CsvHelper;
using HotelManager.DAL;
using HotelManager.Helpers;
using HotelManager.Helpers.Providers;
using HotelManager.Models.Providers;
using HotelManager.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using HotelManager.Models;
using System.Data.Entity;

namespace HotelManager.Controllers.Providers
{
    public class ExpediaController : Controller
    {
        private HotelManagerContext db = new HotelManagerContext();

        // GET: Expedia
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var ExpediaHotel = db.ExpediaHotels.SingleOrDefault(p => p.ProviderHotelID == id);

            if (ExpediaHotel == null)
            {
                return HttpNotFound();
            }
            return View(ExpediaHotel);
        }

        [Authorize(Roles = "canImport")]
        public ActionResult ImportFile()
        {

            HotelManagerContext context = new DAL.HotelManagerContext();

            int processedCount = 0;
            int unchangedCount = 0;
            List<ExpediaHotel> expediaHotels = new List<ExpediaHotel>();
            List<ActivePropertyList> fails = new List<ActivePropertyList>();
            List<String> errorMessages = new List<String>();

            string[] postCodeAreas = new string[] { "AB", "DD", "DG", "EH", "FK", "G", "", "HS", "IV", "KA", "KW", "KY", "ML", "PA", "PH", "TD", "ZE" };

            try
            {
                System.Diagnostics.Debug.Write("Expedia");
                var ImportFile = ConfigurationManager.AppSettings["IMPORT_DIR"] + "ActivePropertyList.txt";
                using (TextReader reader = System.IO.File.OpenText((@ImportFile)))
                {

                    var csv = new CsvReader(reader);
                    // Default value
                    csv.Configuration.Delimiter = "|";
                    var records = csv.GetRecords<ActivePropertyList>();

                    foreach (var item in records)
                    {
                        var test = item.Address2.ToString().ToLower() + " " + item.City.ToString().ToLower();
                        if (item.Country == "GB" && postCodeAreas.Contains(presentationHelper.getPostalArea(item.PostalCode)) || test.IndexOf("corralejo") > 0)
                        {
                            
                            processedCount++;
                            try
                            {
                                ExpediaHotel hotel = new ExpediaHotel();
                                hotel.ProviderID = 2; // value for Expedia
                                hotel.ProviderHotelID = Convert.ToInt32(item.EANHotelID);
                                //hotel.sequence_number = Convert.ToInt32(item.SequenceNumber);
                                hotel.HotelName = item.Name;
                                hotel.AddressLine1 = item.Address1;
                                hotel.AddressLine2 = item.Address2;
                                hotel.TownName = item.City;
                                hotel.RegionName = item.StateProvince;
                                hotel.PostalCode = item.PostalCode;
                                hotel.CountryName = item.Country;
                                hotel.Latitude = float.Parse(item.Latitude);
                                hotel.Longitude = float.Parse(item.Longitude);
                                hotel.NearestAirportCode = item.AirportCode;
                                hotel.StarRating = String.IsNullOrEmpty(item.StarRating) ? 0 : float.Parse(item.StarRating);
                                hotel.ParentChainID = string.IsNullOrEmpty(item.ChainCodeID) ? 0 : Convert.ToInt32(item.ChainCodeID);
                                //hotel.region_id = Convert.ToInt32(item.RegionID);
                                hotel.HighRate = item.HighRate;
                                hotel.LowRate = item.LowRate;
                                hotel.CheckInTime = item.CheckInTime;
                                hotel.CheckOutTime = item.CheckOutTime;
                                hotel.FlaggedForReview = false;
                                hotel.IsClosed = false;
                                hotel.redirectToID = 0;

                                var existingHotel = context.ExpediaHotels
                                    .Where(h => h.ProviderHotelID == hotel.ProviderHotelID)
                                    .FirstOrDefault();

                                if (existingHotel == null)
                                {
                                    expediaHotels.Add(hotel);
                                }
                                else
                                {
                                    var same = ImportHelpers.compareItems(existingHotel, hotel);
                                    if (same.ChangesFound)
                                    {
                                        errorMessages.Add("Existing Hotel with difference - " + hotel.ProviderHotelID);
                                    }
                                    else
                                    { unchangedCount++; }
                                }

                            }
                            catch
                            {
                                fails.Add(item);
                            }
                        }
                    }
                }

            }
            catch
            {
                errorMessages.Add("The file could not be read:");
            }

            expediaHotels.ForEach(s => context.ExpediaHotels.Add(s));
            context.SaveChanges();

            var viewModel = new vmExpediaImport
            {
                ExpediaHotels = expediaHotels,
                Fails = fails,
                ErrorMessages = errorMessages,
                ProcessedCount = processedCount,
                UnchangedCount = unchangedCount
            };

            return View(viewModel);

        }

        [Authorize(Roles = "canImport")]
        public ActionResult ListOrphanedHotels()
        {
            var orphanedHotels = new List<ExpediaHotel>();

            foreach (var item in db.ExpediaHotels.Where(e=> e.InMasterList==false))
            {
                orphanedHotels.Add(item);
            }
            return View(orphanedHotels);
        }

        [Authorize(Roles = "canImport")]
        public ActionResult ListOrphanedHotelsWithPostcodeMatch()
        {
            var orphanedHotels = new List<ExpediaHotel>();

            List<Accommodation> accommodation = new List<Accommodation>();

            foreach (var item in db.Accommodations)
            {
                accommodation.Add(item);
            }

            foreach (var item in db.ExpediaHotels.Where(e => e.InMasterList == false))
            {
                var isFound = false;
                foreach (var accomitem in accommodation)
                {
                    if (accomitem.PostalCode == item.PostalCode)
                    {
                        isFound = true;
                    }

                }

                if (isFound)
                {
                    orphanedHotels.Add(item);
                }
                
            }
            return View("~/Views/Expedia/ListOrphanedHotels.cshtml", orphanedHotels);
        }

        [Authorize(Roles = "canImport")]
        public ActionResult ListOrphanedHotelsFlaggedForReview()
        {
            var orphanedHotels = new List<ExpediaHotel>();

            List<Accommodation> accommodation = new List<Accommodation>();

            foreach (var item in db.ExpediaHotels.Where(e => e.InMasterList == false && e.FlaggedForReview == true))
            {
               

                    orphanedHotels.Add(item);
             

            }
            return View("~/Views/Expedia/ListOrphanedHotels.cshtml", orphanedHotels);
        }

        public ActionResult AddToMasterList(int accommodationID, int expediaID)
        {
            HotelManagerContext context = new HotelManagerContext();
            ExpediaHotel expediaHotel = context.ExpediaHotels.Find(expediaID);

            var added = ExpediaHelpers.AddHotelToMasterList(accommodationID, expediaHotel);

            return View(added);
        }

        public ActionResult AddNewToMasterList(int expediaID)
        {
            HotelManagerContext context = new HotelManagerContext();
            ExpediaHotel expediaHotel = context.ExpediaHotels.Find(expediaID);

            var added = ExpediaHelpers.AddNewHotelToMasterList(expediaHotel);

            return View(added);
        }

        public ActionResult FlagHotelForReview(int expediaID)
        {
            HotelManagerContext context = new HotelManagerContext();
            ExpediaHotel matchedExpediaHotel = context.ExpediaHotels.Find(expediaID);

            if (matchedExpediaHotel != null)
            {
                context.Entry(matchedExpediaHotel).State = EntityState.Modified;

                matchedExpediaHotel.FlaggedForReview = true;

                context.SaveChanges();
                return View(true);
            }
            else
            {
                return View (false);
            }
        }
    }
}