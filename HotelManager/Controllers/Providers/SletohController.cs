﻿using CsvHelper;
using HotelManager.DAL;
using HotelManager.Helpers;
using HotelManager.Models;
using HotelManager.Models.Logging;
using HotelManager.Models.Providers;
using HotelManager.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;

namespace HotelManager.Controllers.Providers
{
    public class SletohController : Controller
    {
        // GET: Sletoh
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "canImport")]
        public ActionResult ImportFile()
        {
            HotelManagerContext context = new DAL.HotelManagerContext();

            //String destString = "Scotland";
            int processedCount = 0;
            int unchangedCount = 0;


            List<Location> locations = context.Locations.ToList();

            List<SletohHotel> sletohHotels = new List<SletohHotel>();

            List<SletohFile> fails = new List<SletohFile>();
            List<String> errorMessages = new List<String>();

            try
            {
                var ImportFile = ConfigurationManager.AppSettings["IMPORT_DIR"] + "sletoh.com_hotels.csv";
                using (TextReader reader = System.IO.File.OpenText((@ImportFile)))
                {

                    var csv = new CsvReader(reader);

                    var records = csv.GetRecords<SletohFile>();


                   

                    foreach (var item in records)
                    {
                        if (locations.Exists(l => l.LocationSearchString == item.region_parent.ToLower()) || locations.Exists(l => l.LocationSearchString == item.region_name.ToLower()))
                        {
                            var matchingLocation = new Location();
                            foreach (var location in locations)
                            {
                                if (location.LocationSearchString == item.region_parent.ToLower() || location.LocationSearchString ==  item.region_name.ToLower())
                                {
                                    matchingLocation = location;
                                }
                            }
                            processedCount++;
                            try
                            {

                                SletohHotel hotel = new SletohHotel();
                                hotel.ProviderID = 1;// Value for Sletoh
                                hotel.ProviderHotelID = Convert.ToInt32(item.hotel_id);
                                hotel.HotelName = item.hotel_name;
                                hotel.AddressLine1 = item.street;
                                hotel.AddressLine2 = "";
                                hotel.PostalCode = item.zip;
                                //hotel.region_id = Convert.ToInt32(item.region_id);
                                hotel.TownName = item.region_name;
                                //hotel.region_parent = item.region_parent;
                                hotel.CountryName = item.country_name;
                                hotel.Latitude = float.Parse(item.latitude);
                                hotel.Longitude = float.Parse(item.longitude);
                                hotel.ProviderUrl = item.url.Replace("http://localhost/platform/", "http://www.sletoh.com/");
                                hotel.StarRating = (item.star_rating != "" && item.star_rating != null) ? Convert.ToInt32(item.star_rating) : -1;
                                hotel.IsClosed = false;
                                hotel.redirectToID = 0;
                                //hotel.thumbnail_url = item.thumbnail_url.Replace("http://localhost/platform/", "http://www.sletoh.com/");
                                //hotel.photo_url = item.photo_url.Replace("http://localhost/platform/", "http://www.sletoh.com/");

                                hotel.TownID = Convert.ToInt32(item.region_id);
                                Town city = context.Towns.Where(p => p.SletohID.ToString() == item.region_id).FirstOrDefault();

                                if (city == null)
                                {


                                    Town newDest = new Town
                                    {
                                        RegionID = 16,
                                        Slug = presentationHelper.Slugify(item.region_name),
                                        TownName = item.region_name,
                                        TownSummary = "",
                                        TownDescription = "",
                                        SletohID = Convert.ToInt32(item.region_id)
                                    };

                                    ImportChanges changeLog = new ImportChanges
                                    {
                                        Provider = "1",
                                        item_ID = 0,
                                        FieldName = "",
                                        OriginalValue = "",
                                        NewValue = item.region_id + " : " + item.region_name

                                    };

                                    context.ImportChanges.Add(changeLog);
                                    context.Towns.Add(newDest);
                                    context.SaveChanges();

                                }


                                var existingHotel = context.SletoHotels
                                    .Where(h => h.ProviderHotelID == hotel.ProviderHotelID)
                                    .FirstOrDefault();

                                if (existingHotel == null)
                                {
                                    sletohHotels.Add(hotel);
                                }
                                else
                                {
                                    var same = ImportHelpers.compareItems(existingHotel, hotel);
                                    if (same.ChangesFound)
                                    {
                                        same.Changes.ForEach(s => context.ImportChanges.Add(s));
                                        context.SaveChanges();
                                        errorMessages.Add("Existing Hotel with difference - " + hotel.ProviderHotelID);
                                    }
                                    else
                                    { unchangedCount++; }
                                }

                            }
                            catch
                            { fails.Add(item); }
                        }

                    }
                }

            }
            catch
            {
                errorMessages.Add("The file could not be read:");
            }

            sletohHotels.ForEach(s => context.SletoHotels.Add(s));
            context.SaveChanges();

            var viewModel = new vmSletohImport
            {
                SletohHotels = sletohHotels,
                Fails = fails,
                ErrorMessages = errorMessages,
                ProcessedCount = processedCount,
                UnchangedCount = unchangedCount
            };

            return View(viewModel);
        }
    }
}