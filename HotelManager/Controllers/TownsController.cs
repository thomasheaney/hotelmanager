﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelManager.DAL;
using HotelManager.Models;

namespace HotelManager.Controllers
{
    public class TownsController : Controller
    {
        private HotelManagerContext db = new HotelManagerContext();

        // GET: Towns
        [AllowAnonymous]
        public ActionResult Index()
        {
           
            return View();
        }

        // GET: Towns/Details/5
        [AllowAnonymous]
        public ActionResult Details(string slug)
        {
            if (slug == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Town town = db.Towns.SingleOrDefault(p => p.Slug.ToLower().Equals(slug.ToLower()));
            if (town == null)
            {
                return HttpNotFound();
            }


            Region region = db.Regions.Find(town.RegionID);
            if (region == null)
            {
                town.RegionSlug = "";
                town.RegionName = "";
            }
            else
            {
                town.RegionSlug = region.Slug;
                town.RegionName = region.RegionName;
            }


            return View(town);
        }

        // GET: Towns/Create
        [Authorize(Roles = "canEdit")]
        public ActionResult Create()
        {
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Slug");
            return View();
        }

        // POST: Towns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "canEdit")]
        public ActionResult Create([Bind(Include = "ID,Slug,RegionID,RegionSlug,RegionName,TownName,TownSummary,TownDescription,SletohID,Show")] Town town)
        {
            if (ModelState.IsValid)
            {
                db.Towns.Add(town);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Slug", town.RegionID);
            return View(town);
        }

        // GET: Towns/Edit/5
        [Authorize(Roles = "canEdit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Town town = db.Towns.Find(id);
            if (town == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Slug", town.RegionID);
            return View(town);
        }

        // POST: Towns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "canEdit")]
        public ActionResult Edit([Bind(Include = "ID,Slug,RegionID,RegionSlug,RegionName,TownName,TownSummary,TownDescription,SletohID,Show")] Town town)
        {
            if (ModelState.IsValid)
            {
                db.Entry(town).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Slug", town.RegionID);
            return View(town);
        }

        // GET: Towns/Delete/5
        [Authorize(Roles = "canEdit")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Town town = db.Towns.Find(id);
            if (town == null)
            {
                return HttpNotFound();
            }
            return View(town);
        }

        // POST: Towns/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "canEdit")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Town town = db.Towns.Find(id);
            db.Towns.Remove(town);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
