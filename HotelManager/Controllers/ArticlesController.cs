﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotelManager.Controllers
{
    public class ArticlesController : Controller
    {
        // GET: Articles
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Details(string articleSlug)
        {
            var vm = new vmArticle
            {
                ArticleSlug = articleSlug
            };
            return View(vm);
        }
    }
}

public class vmArticle
{
    public string ArticleSlug;
}