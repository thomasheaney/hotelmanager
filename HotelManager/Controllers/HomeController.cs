﻿using HotelManager.DAL;
using HotelManager.Models.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotelManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            /*HotelManagerContext context = new DAL.HotelManagerContext();
            List<ExpediaHotel> expediaHotels = new List<ExpediaHotel>();
            List<SletohHotel> sletohHotels = new List<SletohHotel>();
            foreach (var item in context.ExpediaHotels)
            {
                expediaHotels.Add(item);
            }

            foreach (var item in context.SletoHotels)
            {
                sletohHotels.Add(item);
            }


            var viewModel = new vmHome
            {
                SletohHotels = sletohHotels,
                ExpediaHotels = expediaHotels
            };

            */
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}


public class vmHome
{
    public  List<ExpediaHotel> ExpediaHotels {get; set;}
    public List<SletohHotel> SletohHotels { get; set; }
}