﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models.Tasks
{
    public class AccommodationTask : BaseTask
    {
        public int ID { get; set; }

        public int AccommodationID { get; set; }
        public virtual Accommodation Accommodation { get; set; }
    }
}