﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HotelManager.Models.Tasks
{
    public class BaseTask
    {
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public int status { get; set; }

        [DefaultValue(2)]
        public int priority { get; set; }

    }


}




