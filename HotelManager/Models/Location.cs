﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Location
    {
        public int ID { get; set; }
        public string Slug { get; set; }
        public string LocationName { get; set; }
        public string LocationSearchString { get; set; }

        public virtual ICollection<Region> Regions { get; set; }
    }
}