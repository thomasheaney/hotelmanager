﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Website
    {
        public int ID { get; set; }
        public string WebsiteCode { get; set; }
        public string WebsiteName { get; set; }
        public virtual ICollection<Accommodation> Accommodations { get; set; }
    }
}