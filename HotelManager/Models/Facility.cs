﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Facility
    {
        public int ID { get; set; }

        public string FacilityName { get; set; }

        public virtual ICollection<Accommodation> Accommodations { get; set; }
    }
}