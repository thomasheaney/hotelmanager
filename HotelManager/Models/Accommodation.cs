﻿using HotelManager.Models.Providers;
using HotelManager.Models.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Accommodation
    {
        public int ID { get; set; }
        
        public string PageSlug { get; set; }
        public string AccommodationName { get; set; }
        public string AddressLine1 { get; set; }
	    public string AddressLine2 { get; set; }
        public string TownName{ get; set; }
        public string PostalCode{ get; set; }
	
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public float? StarRating { get; set; }
        public string AccommodationSummary { get; set; }
        public string AccommodationDescription { get; set; }

        public string NearestAirportCode { get; set; }
        public int? ParentChainID { get; set; }
        public string HighRate { get; set; }
        public string LowRate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
	
        public string SletohUrl { get; set; }
	    public string ExpediaUrl { get; set; }

        public string GooglePlacesID { get; set; }

        //Contact details
        public string AccommodationWebsite { get; set; }
        public string AccommodationFacebookLink { get; set; }
        public string AccommodationTwitterLink { get; set; }
        public string AccommodationContactEmail { get; set; }

        public bool Published{ get; set; }
        public bool FlaggedForReview { get; set; }

        public bool IsClosed { get; set; }
        public int redirectToID { get; set; } 
	
        // Associations
        public virtual int SletohHotel { get; set; }
        public virtual int ExpediaHotel { get; set; }

        public int TownID { get; set; }	
        public virtual Town Town { get; set; }

        public virtual ICollection<AccommodationTask> Tasks { get; set; }
        public virtual ICollection<Facility> Facilities { get; set; }
        public virtual ICollection<Website> Websites { get; set; }

       

        /*public int TownID { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string AddressStreet { get; set; }
        public string AddressTown { get; set; }
        public string AddressPostcode { get; set; }
        public string TownSlug { get; set; }
        public string TownName { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public float? StarRating { get; set; }
        public string Summary { get; set; }
        public string Desctiption { get; set; }
        public string mainThumbnail { get; set; }
        public string mainImage { get; set; }
        public string sletohLink { get; set; }

        public bool show { get; set; }
        public bool flag_for_review { get; set; }

        public virtual int SletohHotel { get; set; }
        public virtual int ExpediaHotel { get; set; }

        public virtual Town Town { get; set; }
        public virtual ICollection<AccommodationTask> Tasks { get; set; }

        //public virtual ICollection<Review> Reviews { get; set; }
         * */
    }
}