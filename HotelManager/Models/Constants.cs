﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Constants
    {
        public static Dictionary<int, string> TaskStatusList = new Dictionary<int, string>()
        {
            {0, "New"},
            {10, "In Progress"},
            {100, "Complete"},
        };

        public static Dictionary<int, string> TaskPriorityList = new Dictionary<int, string>()
        {
            {1, "Low"},
            {2, "Normal"},
            {3, "High"},
            {4, "Critical"}
        };
    }
}