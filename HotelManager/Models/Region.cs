﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Region
    {
        public int ID { get; set; }
        public string Slug { get; set; }
        public int LocationID { get; set; }
        public string RegionName { get; set; }
        public string RegionSummary { get; set; }
        public string RegionDescription { get; set; }

        public virtual Location Location { get; set; }

        public virtual ICollection<Town> Towns { get; set; }
    }
}