﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models
{
    public class Town
    {
        public int ID { get; set; }
        public string Slug { get; set; }
        public int RegionID { get; set; }
        public string RegionSlug { get; set; }
        public string RegionName { get; set; }
        public string TownName { get; set; }
        public string TownSummary { get; set; }
        public string TownDescription { get; set; }
        public int SletohID { get; set; }
        public bool Show { get; set; }

        public virtual Region Region { get; set; }
        public virtual ICollection<Accommodation> Accommodations { get; set; }
    }
}