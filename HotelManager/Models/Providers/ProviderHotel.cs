﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models.Providers
{
    public abstract class ProviderHotel
    {
        public int ID { get; set; }
        public int ProviderID { get; set; }
        public int ProviderHotelID { get; set; }
        public string HotelName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string TownName { get; set; }
        public string RegionName { get; set; }
        public string PostalCode { get; set; }
        public string CountryName { get; set; }
        public int TownID { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public string NearestAirportCode { get; set; }
        public float? StarRating { get; set; }
        public string ProviderUrl { get; set; }
        public int? ParentChainID { get; set; }
        public string HighRate { get; set; }
        public string LowRate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }

        public bool FlaggedForReview { get; set; }
        public bool InMasterList { get; set; }
        public bool IsClosed { get; set; }
        public int redirectToID { get; set; }

        /*public int provider_id { get; set; }
        public int hotel_id { get; set; }
        public string hotel_name { get; set; }
        public string name { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city_name { get; set; }
        public string state_province { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
        public int city_id { get; set; }
        public float? latitude { get; set; }
        public float? longitude { get; set; }
        public string airport_code { get; set; }
        public float? star_rating { get; set; }
        public string provider_url { get; set; }
        public string thumbnail_url { get; set; }
        public string photo_url { get; set; }
        public int? chain_code_id { get; set; }
        public string highrate { get; set; }
        public string lowrate { get; set; }
        public string check_in_time { get; set; }
        public string check_out_time { get; set; }
        public bool in_master_list { get; set; }*/
    }
}