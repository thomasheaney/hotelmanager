﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models.Providers
{
    public class HotelProvider
    {
        public int ID { get; set; }
        public int ProviderID { get; set; }
        public string ProviderName { get; set; }
    }
}