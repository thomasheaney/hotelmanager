﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManager.Models.Logging
{
    public class ImportChanges
    {
        public int ID { get; set; }
        public int item_ID { get; set; }
        public string Provider { get; set; }
        public string FieldName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }
}