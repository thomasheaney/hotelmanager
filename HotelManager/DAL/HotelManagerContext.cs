﻿using HotelManager.Models;
using HotelManager.Models.Logging;
using HotelManager.Models.Providers;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace HotelManager.DAL
{
    public class HotelManagerContext : DbContext
    {
        public HotelManagerContext() : base("HotelManagerContext")
        {
        }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Town> Towns { get; set; }
        public DbSet<Accommodation> Accommodations { get; set; }

        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Website> Websites { get; set; }

        public DbSet<HotelProvider> Providers { get; set; }
        public DbSet<SletohHotel> SletoHotels { get; set; }
        public DbSet<ExpediaHotel> ExpediaHotels { get; set; }
        public DbSet<HotelManager.Models.Tasks.AccommodationTask> AccommodationTasks { get; set; }

        public DbSet<ImportChanges> ImportChanges { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Accommodation>()
               .HasMany(c => c.Facilities).WithMany(i => i.Accommodations)
               .Map(t => t.MapLeftKey("AccommodationID")
               .MapRightKey("FacilityID")
               .ToTable("AccommodationFacility"));

            modelBuilder.Entity<Accommodation>()
              .HasMany(c => c.Websites).WithMany(i => i.Accommodations)
              .Map(t => t.MapLeftKey("AccommodationID")
              .MapRightKey("WebsiteID")
              .ToTable("AccommodationWebsite"));

            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id).ToTable("AspNetRoles");
            modelBuilder.Entity<IdentityUser>().ToTable("AspNetUsers");
            modelBuilder.Entity<IdentityUserLogin>().HasKey(l => new { l.UserId, l.LoginProvider, l.ProviderKey }).ToTable("AspNetUserLogins");
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId }).ToTable("AspNetUserRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("AspNetUserClaims");

        }

        
    }
}